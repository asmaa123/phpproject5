<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use App\Season;
use App\StudentArchive;
use App\Guardian;
use App\Center;
use App\Course;
use App\Level;
use App\Material;
use App\Payment;
use Carbon\Carbon;
use App\StudentDocument;
use Response;
use Illuminate\Support\Facades\Input;
use Config;
use Session;
use Auth;
use Hash;
use DB;


class StudentsController extends Controller
{
    public function getIndex() {
        $seasons = Season::where("active", 1)->get();
        $students = DB::table('students')
                ->join('centers','centers.id','=','students.center_id')
                ->join('seasons','seasons.id','=','students.season_id')
                ->join('counts','counts.student_id','=','students.id')
                ->select('students.*','centers.center_name','seasons.season_name','counts.amount')
                ->where("students.guardian_id",'=', Auth::guard('members')->user()->id)
                ->where("students.active", 1)
                ->get();
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
                ->join('students','student_documents.student_id','=','students.id')
                ->select('student_documents.*','students.student_name')
                ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->get();
        $now = Carbon::now();
        return view('site.pages.student.index', compact('seasons','now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }

    public function report() {
        $students = Student::where("active", 1)->get();
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
                ->join('students','student_documents.student_id','=','students.id')
                ->select('student_documents.*','students.student_name')
                ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->get();
        $now = Carbon::now();
        return view('site.pages.student.report', compact('now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }

    public function getAdd() {
        $seasons = Season::where("active", 1)->get();
        $students = Student::where("active", 1)->get();
        $guardians = Guardian::where("active", 1)->get();
        $centers = Center::where("active", 1)->get();
        $courses = Course::where("active", 1)->get();
        $levels = Level::where("active", 1)->get();
        $docs = DB::table('student_documents')
            ->join('students','student_documents.student_id','=','students.id')
            ->select('student_documents.*','students.student_name')
            ->get();
        $materials = Material::where("active", 1)->get();
        $mats = DB::table('student_materials')
            ->join('students','student_materials.student_id','=','students.id')
            ->join('materials','student_materials.material_id','=','materials.id')
            ->select('student_materials.*','students.student_name','materials.material_name')
            ->get();
        $now = Carbon::now();
        return view('site.pages.student.add', compact('seasons','now','courses','students','docs','guardians','centers','levels','materials','mats'));
    }


    public function insert(Request $request) {
        $v = validator($request->all() ,[
            'image' => 'image|mimes:jpeg,jpg,png,gif,pdf|max:20000',
            'name' => 'required',
            'birth' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'first_day' => 'required',
            'national_id' => 'required',
            'guardian_id' => 'required',
            'nationality' => 'required',
            'center_id' => 'required',
            'year' => 'required',
            'season_id' => 'required',
        ] ,[
            'image.image' => 'من فضلك حمل صورة وليس فيديو',
            'image.mimes' => 'يرجى تحميل ملفات بصيغة  JPG,PNG,GIF,PDF',
            'image.max' => 'الحد الاقصى لحجم الملف : 20 MB',
            'name.required' => 'من فضلك أدخل اسم الطالب',
            'birth.required' => 'من فضلك أدخل تاريخ الميلاد',
            'address.required' => 'من فضلك أدخل العنوان',
            'gender.required' => 'من فضلك أدخل الجنس',
            'first_day.required' => 'من فضلك أدخل يوم الالتحاق',
            'national_id.required' => 'من فضلك أدخل الرقم المدنى',
            'center_id.required' => 'من فضلك اختر المركز',
            'guardian_id.required' => 'من فضلك اختر ولى الأمر',
            'nationality.required' => 'من فضلك أدخل الجنسية',
            'season_id.required' => 'من فضلك اختر الفصل الدراسي',
            'year.required' => 'من فضلك أدخل السنة الدراسية',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $student = new Student();

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $student->image = $imageName;
        }

        $student->student_name = $request->name;
        $student->birth = $request->birth;        
        $student->age = Carbon::parse($request->birth)->age;
        $student->address = $request->address;
        $student->email = $request->email;
        $student->first_day = $request->first_day;
        $student->gender = $request->gender;
        $student->national_id = $request->national_id;
        $student->guardian_id = Auth::guard('members')->user()->id;
        $student->center_id = $request->center_id;
        $student->nationality = $request->nationality;
        $student->season_id = $request->season_id;
        $student->year = $request->year;
        $student->active = 2;
        if(isset($request->transportation)){
            $student->transportation = 1;
        }
        $centers = Center::get()->where('id','=',$request->input('center_id'));
        $age = Carbon::parse($request->input('birth'))->age;
        foreach($centers as $center){
            if($center->age > $age){
                return ['status' => false ,'data' => 'حدث خطأ , سن الطالب اقل من سن القبول '];
            }
        }
        
        if ($student->save()){
            $archive = new StudentArchive();
            $archive->student_id = $id;
            $archive->center_id = $request->center_id;
            $archive->level_id = $request->level_id;
            $archive->season_id = $request->season_id;
            $archive->year = $request->year;
            $now = Carbon::now();
            $price = DB::table('seasons')
                    ->join('students','students.season_id','=','seasons.id')
                    ->where('students.id','=',$student->id)
                    ->sum('seasons.price');
            $amount = 0;
            $search = DB::table('counts')
                    ->select('*')
                    ->where('student_id','=',$student->id)
                    ->first();

            $prev = DB::table('counts')
                    ->where('student_id','=',$student->id)
                    ->sum('amount');

            $notes = 'مصروفات الطالب '.$student->student_name.'';
            if($search){
                $amount = $prev - $price;
                $data = array(
                    'amount'=>$amount,
                    'date'=>$now,
                    );
                DB::table('counts')->where('student_id',$id)->update($data);
            }else{
                $amount = $amount - $price;
                $data = array(
                    'student_id'=>$student->id,
                    'amount'=>$amount,
                    'date'=>$now,
                    'notes'=>$notes
                    );
                DB::table('counts')->insert($data);
            }
            $archive->save();
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function getEdit($id) {
        if (isset($id)) {
            $students = Student::where("active", 1)->get();
            $seasons = Season::where("active", 1)->get();
            $student = Student::find($id);
            $archives = DB::table('student_archives')
                    ->join('students','student_archives.student_id','=','students.id')
                    ->join('centers','student_archives.center_id','=','centers.id')
                    ->join('seasons','student_archives.season_id','=','seasons.id')
                    ->select('student_archives.*','students.student_name','centers.center_name','seasons.season_name')
                    ->where('student_archives.student_id','=',$id)
                    ->get();
            $guardians = Guardian::where("active", 1)->get();
            $centers = Center::where("active", 1)->get();
            $courses = Course::where("active", 1)->get();
            $cours = DB::table('student_courses')
                    ->join('students','student_courses.student_id','=','students.id')
                    ->join('courses','student_courses.course_id','=','courses.id')
                    ->select('student_courses.*','students.student_name','courses.course_name')
                    ->where('student_courses.student_id','=',$id)
                    ->get();
            $levels = Level::where("active", 1)->get();
            $docs = DB::table('student_documents')
            ->select('student_documents.*')
            ->where('student_id','=', $id)
            ->get();
            $materials = Material::where("active", 1)->get();
            $mats = DB::table('student_materials')
                    ->join('students','student_materials.student_id','=','students.id')
                    ->join('materials','student_materials.material_id','=','materials.id')
                    ->select('student_materials.*','students.student_name','materials.material_name')
                    ->where('student_materials.student_id','=',$id)
                    ->get();
            return view('site.pages.student.edit', compact('seasons','archives','cours','courses','materials','mats','student','students','docs','guardians','centers','levels'));
        }        
    }

    public function postEdit(Request $request,$id) {
        
        $student = Student::find($id);
        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $student->image = $imageName;
        }
        $student->student_name = $request->name;
        $student->season_id = $request->season_id;
        $student->birth = $request->birth;
        $student->age = Carbon::parse($request->birth)->age;
        $student->address = $request->address;
        $student->email = $request->email;
        $student->first_day = $request->first_day;
        $student->gender = $request->gender;
        $student->national_id = $request->national_id;
        $student->level_id = $request->level_id;
        $student->guardian_id = Auth::guard('members')->user()->id;
        $student->center_id = $request->center_id;
        $student->nationality = $request->nationality;
        $student->year = $request->year;
        $student->active = 3;
        if(isset($request->transportation)){
            $student->transportation = 1;
        }
        $centers = Center::get()->where('id','=',$request->input('center_id'));
        $age = Carbon::parse($request->input('birth'))->age;
        foreach($centers as $center){
            if($center->age > $age){
                return ['status' => false ,'data' => 'حدث خطأ , سن الطالب اقل من سن القبول '];
            }
        }

        $search = DB::table('student_archives')
                    ->join('students','students.id','=','student_archives.student_id')
                    ->select('*')
                    ->where('student_id','=',$id)
                    ->where('student_archives.season_id','=',$student->season_id)
                    ->first();
            
        if ($student->save()){
            if(!$search){
                $archive = new StudentArchive();
                $archive->student_id = $id;
                $archive->center_id = $request->center_id;
                $archive->level_id = $request->level_id;
                $archive->season_id = $request->season_id;
                $archive->year = $request->year;
                $now = Carbon::now();
                $price = DB::table('seasons')
                        ->join('students','students.season_id','=','seasons.id')
                        ->where('students.id','=',$id)
                        ->sum('seasons.price');
                $amount = 0;
                $search2 = DB::table('counts')
                        ->select('*')
                        ->where('student_id','=',$id)
                        ->first();

                $prev = DB::table('counts')
                        ->where('student_id','=',$id)
                        ->sum('amount');

                $notes = 'مصروفات الطالب '.$student->student_name.'';
                if($search2){
                    $amount = $prev - $price;
                    $data = array(
                        'amount'=>$amount,
                        'date'=>$now,
                        );
                    DB::table('counts')->where('student_id',$id)->update($data);
                }else{
                    $amount = $amount - $price;
                    $data = array(
                        'student_id'=>$id,
                        'amount'=>$amount,
                        'date'=>$now,
                        'notes'=>$notes
                        );
                    DB::table('counts')->insert($data);
                }
                $archive->save();
            }
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }

    public function delete($id) {
        if (isset($id)) {
            $student = Student::find($id);
            $student->delete();
            DB::table('student_materials')->where('student_id','=', $id)->delete();
            DB::table('student_documents')->where('student_id','=', $id)->delete();
            DB::table('student_courses')->where('student_id','=', $id)->delete();
            DB::table('student_grades')->where('student_id','=', $id)->delete();

            return redirect()->back();
        }
    }

}
