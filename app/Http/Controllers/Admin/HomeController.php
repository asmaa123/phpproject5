<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use App\StudentCourse;
use App\StudentMaterial;
use App\StudentLevel;
use App\TeacherLevel;
use App\TeacherMaterial;
use App\CourseMaterial;
use App\Org;
use App\Guardian;
use App\Center;
use App\Teacher;
use App\Course;
use App\Absent;
use App\Level;
use App\Type;
use App\User;
use App\Material;
use App\Percent;
use App\TeacherCourse;
use App\StudentGrade;
use App\Payment;
use App\Paym;
use Carbon\Carbon;
use App\Student_document;
use Response;
use Illuminate\Support\Facades\Input;
use Config;
use Session;
use Auth;
use Hash;
use App\CourseType;
use DB;
use validator;

class HomeController extends Controller
{
    public function getIndex(Request $request) {


        
        
        
        

       

        return view('admin.pages.home', compact('teachers','now','org','students','docs','guardians','centers','levels','materials','mats','courses','teachers','typesWithCourses'));
    }

    public function error() {
        return view('admin.pages.error');
    }
//----------------------------------------------------------------------------------------------------------
public function getNCourse() {

    $center_id = Input::get('center_id');

    $courses = Course::where('center_id','=',$center_id)->get();

    return Response::json($courses);
}

public function getNMaterial() {

    $level_id = Input::get('level_id');

    $materials = DB::table('level_materials')
            ->join('levels','level_materials.level_id','=','levels.id')
            ->join('materials','level_materials.material_id','=','materials.id')
            ->select('level_materials.*','levels.level_name','materials.material_name')
            ->where('level_materials.level_id','=',$level_id)
            ->get();

    return Response::json($materials);
}

public function getNLevel() {

    $course_id = Input::get('course_id');

   /* $levels = DB::table('course_levels')
            ->join('courses','course_levels.course_id','=','courses.id')
            ->join('levels','course_levels.level_id','=','levels.id')
            ->select('course_levels.*','courses.course_name','levels.level_name')
            ->where('course_levels.course_id','=',$course_id)
            ->get();*/
            
              $levels = DB::table('levels')
            ->join('courses','courses.id','levels.course_id')
           // ->select('course_levels.*','courses.course_name','levels.level_name')
           ->where('courses.id','=',2)
            ->get();
    
    return Response::json($levels);
}

public function addcoursematerial(Request $request) {

    $v = validator($request->all() ,[
        'center_id' => 'required',
        'course_id' => 'required',
        'level_id' => 'required',
        'coursetype_id'=>'required'
    ] ,[
        'center_id.required' => 'من فضلك اختر مركز',
        'course_id.required' => 'من فضلك اختر حلقة',
        'level_id.required' => 'من فضلك اختر موظف',
         'coursetype_id.required' => 'من فضلك اخترنوع حلقه'
    ]);

    if ($v->fails()){
        return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
    }
    $materials = DB::table('level_materials')
            ->join('levels','level_materials.level_id','=','levels.id')
            ->join('materials','level_materials.material_id','=','materials.id')
            ->select('level_materials.*','levels.level_name','materials.material_name')
            ->where('level_materials.level_id','=',$request->level_id)
            ->get();
    foreach($materials as $m){
        $material = new CourseMaterial();
        $materials = $request->input('mat'.$m->material_id);
        if(isset($materials)){
            $material->course_id = $request->course_id;
            $material->material_id = $m->material_id;
        }
        $material->save();
    }

    return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
}

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
public function getNewCourse() {

    $center_id = Input::get('center_id');

    $courses = Course::where('center_id','=',$center_id)->get();

    return Response::json($courses);
}

public function getNewTeacher() {

    $teachers = Teacher::all();

    return Response::json($teachers);
}

public function getNewMaterial() {

    $level_id = Input::get('level_id');

    $materials = DB::table('level_materials')
            ->join('levels','level_materials.level_id','=','levels.id')
            ->join('materials','level_materials.material_id','=','materials.id')
            ->select('level_materials.*','levels.level_name','materials.material_name')
            ->where('level_materials.level_id','=',$level_id)
            ->get();

    return Response::json($materials);
}

public function getNewLevel() {

    $course_id = Input::get('course_id');

   /* $levels = DB::table('course_levels')
            ->join('courses','course_levels.course_id','=','courses.id')
            ->join('levels','course_levels.level_id','=','levels.id')
            ->select('course_levels.*','courses.course_name','levels.level_name')
          // ->where('levels.course_id','=',$course_id)
           ->where('course_levels.course_id','=',$course_id)
            ->get();*/
            
            $levels = DB::table('courses')
            ->join('levels','levels.course_id','courses.id')
            ->select('levels.level_name', 'levels.id')
           ->where('courses.id','=',$course_id)
            ->get();
            
            
    
    return Response::json($levels);
}

public function addnewteacher(Request $request) {

    $v = validator($request->all() ,[
        'center_id' => 'required',
        'teacher_id' => 'required',
        'course_id' => 'required',
        'level_id' => 'required',
        'coursetype_id' => 'required'
    ] ,[
        'center_id.required' => 'من فضلك اختر مركز',
        'teacher_id.required' => 'من فضلك اختر طالب',
        'course_id.required' => 'من فضلك اختر حلقة',
        'level_id.required' => 'من فضلك اختر موظف',
        'coursetype_id.required' => 'من فضلك نوتع حلقه',
    ]);

    if ($v->fails()){
        return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
    }

    $course = new TeacherCourse();
    $course->course_id = $request->course_id;
    $course->teacher_id = $request->teacher_id;
   

    $level = new TeacherLevel();
    $level->level_id = $request->level_id;
    $level->teacher_id = $request->teacher_id;

    $materials = DB::table('level_materials')
            ->join('levels','level_materials.level_id','=','levels.id')
            ->join('materials','level_materials.material_id','=','materials.id')
            ->select('level_materials.*','levels.level_name','materials.material_name')
            ->where('level_materials.level_id','=',$request->level_id)
            ->get();
            
    foreach($materials as $m){
        $material = new TeacherMaterial();
        $teacher = $request->input('mat'.$m->material_id);
        $notes = $request->input('notes'.$m->material_id);
        if(isset($teacher)){
            $material->teacher_id = $request->teacher_id;
            $material->material_id = $m->material_id;
            $material->status = 1;
            $material->notes = $notes;
        }else{
            $material->teacher_id = $request->teacher_id;
            $material->material_id = $m->material_id;
            $material->status = 0;
            $material->notes = $notes;
        }
        $search2 = DB::table('teacher_material')
                ->select('*')
                ->where('teacher_id','=',$request->teacher_id)
                ->where('material_id','=',$m->material_id)
                ->first();
        if($search2){
            return ['status' => false ,'data' => 'حدث خطأ , تم تسجيل المدرس سابقا '];
        }else{
            $material->save();
        }
    }

    $search3 = DB::table('teacher_levels')
                ->select('*')
                ->where('teacher_id','=',$request->teacher_id)
                ->where('level_id','=',$request->level_id)
                ->first();

    $search4 = DB::table('teacher_courses')
                ->select('*')
                ->where('teacher_id','=',$request->teacher_id)
                ->where('course_id','=',$request->course_id)
                ->first();

    if($search3){
        return ['status' => false ,'data' => 'حدث خطأ , تم تسجيل المدرس سابقا '];            
    }
    if($search4){
        return ['status' => false ,'data' => 'حدث خطأ , تم تسجيل المدرس سابقا '];
    }

    if ($course->save() && $level->save()){
        return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
    }else{
        return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
    }
}

//----------------------------------------------------------------------------------------------------------
    public function getAddCourse() {

        $center_id = Input::get('center_id');

        $courses = Course::where('center_id','=',$center_id)->get();

        return Response::json($courses);
    }

    public function getAddStudent() {

        $center_id = Input::get('center_id');

        $students = Student::where('center_id','=',$center_id)->get();

        return Response::json($students);
    }

    public function getAddMaterial() {

		$level_id = Input::get('level_id');

		$materials = DB::table('level_materials')
				->join('levels','level_materials.level_id','=','levels.id')
				->join('materials','level_materials.material_id','=','materials.id')
				->select('level_materials.*','levels.level_name','materials.material_name')
				->where('level_materials.level_id','=',$level_id)
				->get();

        return Response::json($materials);
    }

    public function getAddLevel() {

		$course_id = Input::get('course_id');
/*
	$levels = DB::table('course_levels')
				->join('courses','course_levels.course_id','=','courses.id')
				->join('levels','course_levels.level_id','=','levels.id')
				->select('course_levels.*','courses.course_name','levels.level_name')
				->where('course_levels.course_id','=',$course_id)
				->get();
				*/
				
				 $levels = DB::table('courses')
            ->join('levels','levels.course_id','courses.id')
            ->select('levels.level_name')
           ->where('courses.id','=',$course_id)
            ->get();
		
        return Response::json($levels);
    }

    public function addnewstudent(Request $request ) {
        
 

        $v = validator($request->all() ,[
            'center_id' => 'required',
            'student_id' => 'required',
            'course_id' => 'required',
            'level_id' => 'required',
            'coursetype_id'=>'required'
           
        ] ,[
            'center_id.required' => 'من فضلك اختر مركز',
            'student_id.required' => 'من فضلك اختر طالب',
            'course_id.required' => 'من فضلك اختر حلقة',
            'level_id.required' => 'من فضلك اختر مستوى',    
            
           'coursetype_id.required' => 'من فضلك اختر نوع الحلقه',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

      $course = new StudentCourse();
        $course->course_id = $request->course_id;
        $course->student_id = $request->student_id;



      $coursetype = new Course();
      $coursetype->center_id = $request->center_id;

         $coursetype->coursetype_id	 = $request->coursetype_id	;
    //     $coursetype->save();
        
        
        
       // $type=Type::find($id);
      //  $level=Level::;
       // $level->type_id=$type->id;
        
        
     /*   $students=Student::all();
        $centers=Center::all();
        
*/
        $now = Carbon::now();
                    
        $materials = DB::table('level_materials')
                    ->join('levels','level_materials.level_id','=','levels.id')
                    ->join('materials','level_materials.material_id','=','materials.id')
                    ->select('level_materials.*','levels.level_name','materials.material_name')
                    ->where('level_materials.level_id','=',$request->level_id)
                    ->where('materials.active','=', 1)
                    ->get();
        
        $student = Student::find($request->student_id);
       

        foreach($materials as $m){
            $material = new StudentMaterial();
            $student = $request->input('mat'.$m->material_id);
            $notes = $request->input('notes'.$m->material_id);
            if(isset($student)){
                $material->student_id = $request->student_id;
                $material->material_id = $m->material_id;
                $material->status = 1;
                $material->notes = $notes;
            }else{
                $material->student_id = $request->student_id;
                $material->material_id = $m->material_id;
                $material->status = 0;
                $material->notes = $notes;
            }
           

            $search2 = DB::table('student_materials')
                    ->select('*')
                    ->where('student_id','=',$request->student_id)
                    ->where('material_id','=',$m->material_id)
                    ->first();
            if($search2){
                return ['status' => false ,'data' => 'حدث خطأ , تم تسجيل الطالب سابقا '];
            }else{
                $material->save();
            }
            
        }

        $search4 = DB::table('student_courses')
                ->select('*')
                ->where('student_id','=',$request->student_id)
                ->where('course_id','=',$request->course_id)
                ->first();

        if($search4){
            return ['status' => false ,'data' => 'حدث خطأ , تم تسجيل الطالب سابقا '];
        }

        if ($course->save() && $coursetype->save() ){
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        } 

    }
    
    public function getGroups() {

        $group_id = Input::get('group_id');

        $courses = Course::where('center_id','=',$group_id)->get();

        return Response::json($courses);
    }

    public function getStud() {

        $student_id = Input::get('student_id');

        $students = DB::table('students')
                ->join('student_courses','student_courses.student_id','=','students.id')
                ->select('student_courses.*','students.student_name','students.national_id')
                ->where('student_courses.course_id','=',$student_id)
                ->get();

        return Response::json($students);
    }

    public function dates() {

        $dfrom = Input::get('dfrom');
        $dto = Input::get('dto');
        $center = Input::get('center');
        $course = Input::get('course');

        $students = DB::table('absents')
                ->select('absents.date')
                ->whereBetween('absents.date', [$dfrom, $dto])
                ->where('absents.course_id','=',$course)
                ->where('absents.center_id','=',$center)
                ->groupBy('absents.date')
                ->get();
        

        return Response::json($students);
    }

    public function studs() {

        $dfrom = Input::get('dfrom');
        $dto = Input::get('dto');
        $center = Input::get('center');
        $course = Input::get('course');

        $students = DB::table('absents')
                ->join('students','absents.student_id','=','students.id')
                ->select('students.student_name','absents.student_id')
                ->whereBetween('absents.date', [$dfrom, $dto])
                ->where('absents.course_id','=',$course)
                ->where('absents.center_id','=',$center)
                ->groupBy('absents.student_id')
                ->groupBy('students.student_name')
                ->get();
        

        return Response::json($students);
    }

    public function dfrom() {

        $dfrom = Input::get('dfrom');
        $dto = Input::get('dto');
        $center = Input::get('center');
        $course = Input::get('course');
        $s_id = Input::get('s_id');

        $status = DB::table('absents')
                ->select('absents.status')
                ->whereBetween('absents.date', [$dfrom, $dto])
                ->where('absents.course_id','=',$course)
                ->where('absents.center_id','=',$center)
                ->where('absents.student_id','=',$s_id)
                ->get();
        

        return Response::json($status);
    }

    public function dto() {

        $dfrom = Input::get('dfrom');
        $dto = Input::get('dto');
        $center = Input::get('center');
        $course = Input::get('course');
        $s_id = Input::get('s_id');

        $students = DB::table('absents')
                ->select('absents.status')
                ->whereBetween('absents.date', [$dfrom, $dto])
                ->where('absents.course_id','=',$course)
                ->where('absents.center_id','=',$center)
                ->where('absents.student_id','=',$s_id)
                ->get();
        

        return Response::json($students);
    }

    public function absent(Request $request) {
        $v = validator($request->all() ,[
            'center_id' => 'required',
            'course_id' => 'required',
            'date' => 'required',
        ] ,[
            'course_id.required' => 'من فضلك اختر احدى الحلقات',
            'center_id.required' => 'من فضلك اختر المركز',
            'date.required' => 'من فضلك أدخل التاريخ',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $stud = DB::table('absents')
				->select('absents.*')
                ->where('center_id','=',$request->center_id)
                ->where('course_id','=',$request->course_id)
                ->where('date','=',$request->date)
                ->where('student_id','=',$request->student_id)
				->first();
        if ($stud){
            return ['status' => false ,'data' => 'حدث خطأ , لقد قمت بتسجيل الحضور سابقا'];
        }else{
            $students = DB::table('students')
                    ->join('student_courses','student_courses.student_id','=','students.id')
                    ->select('student_courses.*','students.student_name','students.national_id')
                    ->where('student_courses.course_id','=',$request->course_id)
                    ->get();
            foreach($students as $s){
                $absent = new Absent();

                $absent->center_id = $request->center_id;
                $absent->course_id = $request->course_id;
                $absent->date = $request->date;
                $item = $request->input('ab'.$s->student_id);
                $student = $request->input('st'.$s->student_id);
                if(isset($item)){
                    $absent->student_id = $student;
                    $absent->status = 1;
                }else{
                    $absent->student_id = $student;
                    $absent->status = 0;
                }
                $absent->save();
            }
            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];      
        }
        
    }

    public function getCourse() {

        $course_id = Input::get('course_id');

        $courses = Course::where('center_id','=',$course_id)->get();

        return Response::json($courses);
    }

    public function getStudent() {

        $student_id = Input::get('student_id');

        $students = Student::where('center_id','=',$student_id)->get();

        return Response::json($students);
    }

    public function getMaterial() {

		$material_id = Input::get('material_id');

		$maters = DB::table('student_materials')
				->join('students','student_materials.student_id','=','students.id')
				->join('materials','student_materials.material_id','=','materials.id')
				->select('student_materials.*','students.student_name','materials.material_name','students.national_id')
				->where('student_materials.student_id','=',$material_id)
				->get();
		

        //$maters = Material::where('student_id','=',$material_id)->get();

        return Response::json($maters);
    }

    public function getPercent() {

        $percent_id = Input::get('percent_id');

		$percents = Percent::where('material_id','=',$percent_id)->get();
		
        return Response::json($percents);
    }
    
    public function grades(Request $request) {
        $v = validator($request->all() ,[
            'center_id' => 'required',
            'student_id' => 'required',
            'material_id' => 'required',
            'date' => 'required',
        ] ,[
            'student_id.required' => 'من فضلك اختر احد الطلاب',
            'center_id.required' => 'من فضلك اختر المركز',
            'material_id.required' => 'من فضلك اختر مادة',
            'date.required' => 'من فضلك أدخل تاريخ التقييم',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }
        $stud = DB::table('student_grades')
				->join('student_percents','student_percents.grade_id','=','student_grades.id')
				->select('student_grades.*','student_percents.*')
                ->where('student_grades.student_id','=',$request->student_id)
                ->where('student_grades.date','=',$request->date)
                ->where('student_grades.material_id','=',$request->material_id)
				->get();
        if ($stud){
            return ['status' => false ,'data' => 'حدث خطأ , لقد قمت بتسجيل التقييم سابقا'];
        }else{
            $grade = new StudentGrade();
            $grade->center_id = $request->center_id;
            $grade->student_id = $request->student_id;
            $grade->material_id = $request->material_id;
            $grade->date = $request->date;
            $count = Percent::where('material_id','=',$request->material_id)->get();
            $total = 0;
            foreach($count as $cc){
                $item = $request->input('p'.$cc->id);
                if($item > $cc->grade){
                    return ['status' => false ,'data' => 'حدث خطأ , من فضلك أدخل درجة أقل'];
                }
                if(isset($item)){
                    $total = $total + $item;
                }
            }
            $grade->total = $total;
            $percents = Material::where('id','=',$request->material_id)->get();
            foreach($percents as $pp){
                if($total >= $pp->p1){
                    $grade->percent = "امتياز";
                }elseif($total >= $pp->p2){
                    $grade->percent = "جيد جدا";
                }elseif($total >= $pp->p3){
                    $grade->percent = "جيد";
                }elseif($total >= $pp->p4){
                    $grade->percent = "مقبول";
                }elseif($total >= $pp->p5){
                    $grade->percent = "ضعيف";
                }elseif($total <= $pp->p5){
                    $grade->percent = "راسب";
                }
            }
            
            if ($grade->save()){
                foreach($count as $c){
                    $item = $request->input('p'.$c->id);
                    if(isset($item)){
                        $grade->details()->create([
                            'grade' => $request->input('p'.$c->id),
                            'grade_id' => $grade->id,
                            'material_id' => $request->material_id,
                            'percent_id' => $c->id
                        ]);
                    }
                }
                return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];            
            }else{
                return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
            }
        }
    }
	
	public function postEdit(Request $request) {
    	
        $id = 1;
        $org = Org::find($id);

        $org->name = $request->name;
        $org->address = $request->address;
        $org->business_registration = $request->business_registration;
        $org->tax_card = $request->tax_card;
        $org->phone = $request->phone;
        $org->fax = $request->fax;
        $org->email = $request->email;
        $org->website = $request->website;

        $destination = storage_path('uploads/' . $request->storage);
        $image = $request->file('image');
        if ($image) {
            if (is_file($destination . "/{$image}")) {
                @unlink($destination . "/{$image}");
            }
            $imageName = $image->getClientOriginalName();
            $image->move($destination, $imageName);
            $org->logo = $imageName;
        }
        

        if ($org->save()){
            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];            
        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }
    }
    //----------------------------------------------------------------------------------------------------

    public function getCourses() {

        $course_id = Input::get('courses_id');

        $courses = Course::where('center_id','=',$course_id)->get();

        return Response::json($courses);
    }

    public function getStudents() {

        $student_id = Input::get('students_id');

        $students = DB::table('student_courses')
                ->join('students','student_courses.student_id','=','students.id')
                ->join('courses','student_courses.course_id','=','courses.id')
                ->select('students.*')
                ->where('student_courses.course_id','=',$student_id)
                ->get();

        return Response::json($students);
    }

    public function getMaterials() {

        $material_id = Input::get('materials_id');

        $maters = DB::table('student_materials')
                ->join('students','student_materials.student_id','=','students.id')
                ->join('materials','student_materials.material_id','=','materials.id')
                ->select('materials.*','student_materials.material_id')
                ->where('student_materials.student_id','=',$material_id)
                ->get();
        

        //$maters = Material::where('student_id','=',$material_id)->get();

        return Response::json($maters);
    }
    
    
     public function getType() {
        

        $type_id = Input::get('type_id');
    

        $maters = DB::table('course_types')
                ->join('types','course_types.type_id','=','types.id')
                ->join('courses','course_types.course_id','=','courses.id')
                ->select('types.*','course_types.type_id')
                ->where('course_types.course_id','=', $type_id)
                ->get();
        

        //$maters = Material::where('student_id','=',$material_id)->get();

        return Response::json($maters);
        
        
    }

    public function getSMaterial() {

        $material_id = Input::get('materials_id');

        $materis = DB::table('student_grades')
                ->join('students','student_grades.student_id','=','students.id')
                // ->join('materials','student_grades.material_id','=','materials.id')
                // // ->select('materials.material_name','student_grades.date','student_grades.total','student_grades.id','student_grades.percent')
                // ->where('student_grades.student_id','=',$material_id)
                ->get();
        

        //$maters = Material::where('student_id','=',$material_id)->get();

        return Response::json($materis);
    }

    public function getSPercent() {

        $p_id = Input::get('p_id');

        $materis = DB::table('percents')
                ->join('student_percents','student_percents.percent_id','=','percents.id')
                ->select('percents.percent_name','student_percents.id','student_percents.grade')
                ->where('student_percents.grade_id','=',$p_id)
                ->get();
        

        //$maters = Material::where('student_id','=',$material_id)->get();

        return Response::json($materis);
    }

    public function getPercents() {

        $percent_id = Input::get('percents_id');
        $student_id = Input::get('student_id');

        $percents = DB::table('student_grades')
                ->join('students','student_grades.student_id','=','students.id')
                ->join('materials','student_grades.material_id','=','materials.id')
                ->select('materials.material_name','student_grades.date','student_grades.total','student_grades.id','student_grades.percent')
                ->where('student_grades.material_id','=',$percent_id)
                ->where('student_grades.student_id','=',$student_id)
                ->get();
        
        return Response::json($percents);
    }

    public function getFrom() {

        $from = Input::get('from');
        $now = Carbon::now();
        $percents = DB::table('student_grades')
                    ->join('students','student_grades.student_id','=','students.id')
                    ->join('materials','student_grades.material_id','=','materials.id')
                    ->select('materials.material_name','student_grades.date','student_grades.total','student_grades.id','student_grades.percent')
                    ->whereBetween('student_grades.date', [$from, $now])
                    ->get();
        return Response::json($percents);
    }

    public function getTo() {

        $from = Input::get('from');
        $to = Input::get('to');
        $percents = DB::table('student_grades')
                    ->join('students','student_grades.student_id','=','students.id')
                    ->join('materials','student_grades.material_id','=','materials.id')
                    ->select('materials.material_name','student_grades.date','student_grades.total','student_grades.id','student_grades.percent')
                    ->whereBetween('student_grades.date', [$from, $to])
                    ->get();
        return Response::json($percents);
    }

//--------------------------------------------------------------------------------------------------------
    public function lock() {
        Session::put('locked', true);
        return view('admin.pages.lock');
    }

    public function back(Request $r) {
        // 1- Validator::make()
        // 2- check if fails
        // 3- fails redirect or success not redirect

        $return = [
            'status' => 'success',
            'message' => 'Login Success!',
        ];

        // grapping admin credentials
        $password = $r->input('password');
        // Searching for the admin matches the passed email or adminname
        $admin = User::where('username', Auth::guard('admins')->user()->username)->where('active', 1)->orWhere('email', Auth::guard('admins')->user()->username)->first();
    //($admin && Hash::check($password, $admin->password))
        if ($admin && Hash::check($password, $admin->password)) {
            // login the admin
            Session::forget('locked');
            Auth::guard('admins')->login($admin, $r->has('remember'));
        } else {
            $return = [
                'response' => 'error',
                'message' => 'Login Failed!'
            ];
        }
        return response()->json($return);
    }

//------------------------------------------------------------------------------------------------------------
    public function getPayCourse() {

        $course_id = Input::get('course_id');

        $courses = Course::where('center_id','=',$course_id)->get();

        return Response::json($courses);
    }

    public function getPayStudent() {

        $student_id = Input::get('student_id');

        $students = DB::table('student_courses')
                ->join('students','student_courses.student_id','=','students.id')
                ->join('courses','student_courses.course_id','=','courses.id')
                ->select('students.*')
                ->where('student_courses.course_id','=',$student_id)
                ->get();

        return Response::json($students);
    }

    public function getMaterialPrice() {

        $student_id = Input::get('student_id');

        $price = DB::table('seasons')
                    ->join('students','students.season_id','=','seasons.id')
                    ->select('seasons.price')
                    ->where('students.id','=',$student_id)
                    ->get();
        
        return Response::json($price);
    }

    public function date() {

        $student_id = Input::get('student_id');

        $date = DB::table('payments')->where('student_id','=', $student_id)->max('id');

        $search = DB::table('payments')
                    ->select('payments.*')
                    ->where('student_id','=', $student_id)
                    ->where('id','=', $date)
                    ->get();
        
        return Response::json($search);
    }

    public function process() {

        $student_id = Input::get('student_id');
        $year = Input::get('year');

        $process = DB::table('pay_details')
                    ->select('date','amount')
                    ->whereYear('date', $year)
                    ->where('student_id','=', $student_id)
                    ->get();
        
        return Response::json($process);
    }

    public function month() { 

        $student_id = Input::get('student_id');
        $month = Input::get('month');

        $process = DB::table('pay_details')
                    ->select('date','amount')
                    ->whereMonth('date', $month)
                    ->where('student_id','=', $student_id)
                    ->get();
        
        return Response::json($process);
    }

    public function payms(Request $request) {
        $v = validator($request->all() ,[
            'student_id' => 'required',
            'amount' => 'required',
            'code' => 'required',
        ] ,[
            'student_id.required' => 'من فضلك اختر احد الطلاب',
            'amount.required' => 'من فضلك أدخل المبلغ المراد دفعه',
            'code.required' => 'من فضلك أدخل رقم الايصال',
        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $now = Carbon::now();
        $price = DB::table('seasons')
                ->join('students','students.season_id','=','seasons.id')
                ->where('students.id','=',$request->student_id)
                ->sum('seasons.price');
        $date = DB::table('payments')->where('student_id','=', $request->student_id)->max('id');
        $search = DB::table('payments')
                ->select('payments.*')
                ->where('student_id','=', $request->student_id)
                ->where('id','=', $date)
                ->get();

        $search2 = DB::table('counts')
                ->select('counts.*')
                ->where('student_id','=', $request->student_id)
                ->get();

        
        if($date){
            foreach ($search as $s) {
                $pay = Payment::find($s->id);
                if($pay->remain > 0){
                    return ['status' => false ,'data' => 'حدث خطأ , لقد تم دفع المديونيات سابقا '];
                }else{
                    $payment = new Payment();
                    if($request->discount > 0){
                        $payment->amount = ($request->amount + $request->discount);
                        $payment->remain = ($request->amount + $request->discount)+ $pay->remain;
                        $payment->date = $now;
                        $payment->student_id = $request->student_id;
                        $payment->notes = $request->notes;
                        $payment->code = $request->code;
                        $payment->type = "manual";
                        if($search2){
                            $searchDiscount = (isset($search2->discount)) ?  $search2->discount : "0";
                            $newDiscount = $searchDiscount + $request->discount;
                            $data = array(
                                'amount'=>$payment->remain,
                                'discount'=>$newDiscount
                                );
                            DB::table('counts')->where('student_id','=', $request->student_id)->update($data);
                        }else{
                            $notes = 'مصروفات الطالب '.$student->student_name.'';
                            $data = array(
                                'student_id'=>$request->student_id,
                                'amount'=>$payment->remain,
                                'discount'=>$request->discount,
                                'date'=>$now,
                                'notes'=>$notes
                                );
                            DB::table('counts')->insert($data);
                        }
                        $pay->remain = 0;
                        if ($pay->save() && $payment->save()){
                            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];   
                        }else{
                            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                        }
                    }elseif($request->discount == null){
                        $payment->amount = $request->amount;
                        $payment->remain = ($request->amount + $pay->remain);
                        $payment->date = $now;
                        $payment->student_id = $request->student_id;
                        $payment->notes = $request->notes;
                        $payment->code = $request->code;
                        $payment->type = "manual";
                        if($search2){
                            $data = array(
                                'amount'=>$payment->remain
                                );
                            DB::table('counts')->where('student_id','=', $request->student_id)->update($data);
                        }else{
                            $notes = 'مصروفات الطالب '.$student->student_name.'';
                            $data = array(
                                'student_id'=>$request->student_id,
                                'amount'=>$payment->remain,
                                'discount'=> 0,
                                'date'=>$now,
                                'notes'=>$notes
                                );
                            DB::table('counts')->insert($data);
                        }
                        $pay->remain = 0;
                        if ($pay->save() && $payment->save()){
                            return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];   
                        }else{
                            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                        }
                    }
                }
            }
        }else{
            if($request->discount > 0){
                $payments = new Payment();
                $payments->amount = ($request->amount)-($request->discount);
                if($request->amount > $price){
                    $payments->remain = $request->amount - $price;
                }elseif($request->amount < $price){
                    $payments->remain = $request->amount - $price;
                }elseif($request->amount == $price){
                    $payments->remain = 0;
                }
                $payments->date = $now;
                $payments->student_id = $request->student_id;
                $payments->notes = $request->notes;
                $payments->code = $request->code;
                $payments->type = "manual";
                if($search2){
                    //dd($search2->discount);
                    $searchDiscount = (isset($search2->discount)) ?  $search2->discount : "0";
                    $newDiscount =  $searchDiscount + $request->discount;
                    $data = array(
                        'amount'=>$payments->remain,
                        'discount'=> $newDiscount,
                        );
                    DB::table('counts')->where('student_id','=', $request->student_id)->update($data);
                }else{
                    $notes = 'مصروفات الطالب '.$student->student_name.'';
                    $data = array(
                        'student_id'=>$request->student_id,
                        'amount'=>$payments->remain,
                        'discount'=>$request->discount,
                        'date'=>$now,
                        'notes'=>$notes
                        );
                    DB::table('counts')->insert($data);
                }
                if ($payments->save()){
                    return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];   
                }else{
                    return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                }
            }elseif($request->discount == null){
                $payments = new Payment();
                $payments->amount = $request->amount;
                if($request->amount > $price){
                    $payments->remain = $request->amount - $price;
                }elseif($request->amount < $price){
                    $payments->remain = $request->amount - $price;
                }elseif($request->amount == $price){
                    $payments->remain = 0;
                }
                $payments->date = $now;
                $payments->student_id = $request->student_id;
                $payments->notes = $request->notes;
                $payment->code = $request->code;
                $payment->type = "manual";
                if ($payments->save()){
                    if($search2){
                        $data = array(
                            'amount'=>$payments->remain,
                            'discount'=> 0
                            );
                        DB::table('counts')->where('student_id','=', $request->student_id)->update($data);
                    }else{
                        $notes = 'مصروفات الطالب '.$student->student_name.'';
                        $data = array(
                            'student_id'=>$request->student_id,
                            'amount'=>$payments->remain,
                            'discount'=>$request->discount,
                            'date'=>$now,
                            'notes'=>$notes
                            );
                        DB::table('counts')->insert($data);
                    }
                    return ['status' => 'succes' ,'data' => 'تم تحديث البيانات بنجاح'];   
                }else{
                    return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                }
            }
            
        }
    }

}
