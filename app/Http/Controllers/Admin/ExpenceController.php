<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Expense;
use Config;
use DB;

class ExpenceController extends Controller
{

    public function getIndex(){

       $expenses=Expense::get();


        return view('admin.pages.expenses.index', compact('expenses'));

            }



            public function create(){

                return view('admin.pages.expenses.create');

            }

            public function store(Request $request){


                $v = validator($request->all() ,[

                    'name' => 'required',
                    'details' => 'required',
                    'pay' => 'required',
                    'caching' => 'required',


                ] ,[

                    'name.required' => 'من فضلك أدخل صاحب المصروف',
                    'details.required' => 'من فضلك أدخل التفاصيل ',
                    'pay.required' => 'من فضلك الدفع كام',
                    'caching.required' => 'من فضلك أدخل صرف كام'
                ]);

                if ($v->fails()){
                    return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
                }



                $expense = new Expense;

                $expense->name = $request->name;

                $expense->details = $request->details;

                $expense->pay = $request->pay;

                $expense->caching = $request->caching;

                if ($expense->save()) {

                    return ['status' => 'succes', 'data' => 'تم اضافه البيانات بنجاح'];


                } else {

                    return ['status' => false, 'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                }

            }




            public function getedit($id){


                $expense=Expense::find($id);

                return view('admin.pages.expenses.edit', compact('expense'));


            }



            public function postEdit(Request $request,$id)
            {

                    $expense = Expense::find($id);

                    $expense->name = $request->name;

                    $expense->details = $request->details;

                    $expense->pay = $request->pay;

                    $expense->caching = $request->caching;

                    if ($expense->save()) {

                        return ['status' => 'succes', 'data' => 'تم تحديث البيانات بنجاح'];


                    } else {

                        return ['status' => false, 'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
                    }
                }



                public function destroy($id){

                    $expense=Expense::find($id);

                    $expense->delete();

                    return redirect()->back();

             }


}
