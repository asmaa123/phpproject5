<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\models\Apartment;

use Carbon\Carbon;
use Config;
use DB;

class PaymentController extends Controller
{
    public function getIndex() {

        $payments = Payment::get();
        return view('admin.pages.payment.index', compact('payments'));
    }





    public function create(){
		
		$apartments = Apartment::get();
        return view('admin.pages.payment.create',compact('apartments'));


    }

    public function show($id){

        if(isset($id)) {

            $payments = DB::table('apartment_payment')
                ->join('payments','apartment_payment.payment_id','=','payments.id')
                ->join('apartments','apartment_payment.apartment_id','=','apartments.id')
                ->select('apartment_payment.*','apartments.number','apartments.floor','apartments.owner','apartments.phone')
                ->where('payment_id',$id)
                ->get();




            return view('admin.pages.payment.show',compact('payments'));










        }




    }




    public function store(Request $request)
    {

        $v = validator($request->all() ,[

            'type' => 'required',
            'status' => 'required',
            'year' => 'required',
            'month' => 'required',
            'payment_type' => 'required',
            'amount_paid' => 'required',
            'details' => 'required',
			'apartment' => 'required',


        ] ,[

            'type.required' => 'من فضلك أدخل نوع المصروف',
            'status.required' => 'من فضلك أدخل حاله االدفع ',
            'year.required' => 'من فضلك السنه',
            'month.required' => 'من فضلك أدخه الشهر',
            'payment_type.required' => 'من فضلك أدخله نوع الدفع',
            'amount_paid.required' => 'من فضلك أدخله  الدفع',
            'details.required' => 'من فضلك أدخله التفاصيل',
			'apartment.required' => 'من فضلك الختر الشقة',

        ]);

        if ($v->fails()){
            return ['status' => false , 'data' => implode(PHP_EOL ,$v->errors()->all())];
        }

        $payment=new Payment();

        $payment->type =$request->type;

        $payment->status =$request->status;

        $payment->year =$request->year;

        $payment->month =$request->month;

        $payment->payment_type =$request->payment_type;

        $payment->amount_paid =$request->amount_paid;

        $payment->details =$request->details;
		
		$payment->apartment =$request->apartment;

        if ($payment->save()){

            return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];


        }else{
            return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
        }

       return back();

    }




    public function getEdit($id)
    {
        if(isset($id)) {

            $payment = Payment::find($id);
            $apartments = Apartment::get();


            return view('admin.pages.payment.edit', compact('payment','apartments'));



        }
    }


    public function postEdit(Request $request, $id){




        if(isset($id)) {

            $payment = Payment::find($id);

            $payment->type = $request->type;

            $payment->payment_type = $request->payment_type;

            $payment->status = $request->status;

            $payment->year = $request->year;

            $payment->month = $request->month;

            $payment->amount_paid =$request->amount_paid;

            $payment->details =$request->details;

            $payment->apartment =$request->apartment;

            if ($payment->save()) {

                return ['status' => 'succes', 'data' => 'تم تحديث البيانات بنجاح'];




            } else {
                return ['status' => false, 'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
            }


        }



    }



    public function destroy($id)
    {
        if (isset($id)) {

            $payment = Payment::find($id);

            $payment->delete();

            return redirect()->back();


        }
    }


}
