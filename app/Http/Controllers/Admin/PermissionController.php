<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\SendNewUserInfoMail;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class PermissionController extends Controller
{
  public function get(){

return view('admin.pages.desigh');


  }



  public function insert(Request $request){

  $user=new User;

  $user->username=$request->username;
 $user->email=$request->email;
 $user->password=bcrypt($request->password);

// $user->roles()->attach($request->name);

 $userSaved = $user->save();


if($userSaved){
    $role= Role::findOrFail($request->rolelist);
    $user->attachRole($role);

    foreach ($request->permission as $perm) {
        $permission = Permission::findOrFail($perm);
        if (!$user->can($permission->name)) {
            $role->attachPermission($perm);
        }
    }

    $user->notify(new SendNewUserInfoMail($request->password));
    return ['status' => 'succes' ,'data' => 'تم اضافة البيانات بنجاح'];
}else
    return ['status' => false ,'data' => 'حدث خطأ , من فضلك أعد المحاولة '];
  }




}
