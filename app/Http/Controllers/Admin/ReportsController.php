<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Apartment;
use App\Models\Expense;
use App\Percent;
use Illuminate\Http\Request;

use Config;
use DB;


class ReportsController extends Controller
{

    public function getIndex() {

        $payments = Apartment::get();

        return view('admin.pages.payments.index', compact('payments'));
    }

    public function expenses(){

        $expenses=Expense::get();


        return view('admin.pages.reports.data', compact('expenses'));

    }

public function currentbalance(){

    $expenses=Expense::get();
    $sums = DB::table('expenses')
        ->select(DB::raw('SUM(pay) as total_amount'))
        ->get();
    return view('admin.pages.reports.index', compact('expenses','sums'));

}















}

