<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model 
{

    protected $table = 'payments';
    public $timestamps = true;
    protected $fillable = array('type', 'payment_type', 'amount_paid', 'status', 'year', 'month','apartment');

    public function apartments()
    {
        return $this->belongsToMany('App\Models\Apartment');
    }

}