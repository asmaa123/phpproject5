<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model 
{

    protected $table = 'apartments';
    public $timestamps = true;
    protected $fillable = array('number', 'floor', 'phone', 'owner');

    public function payments()
    {
        return $this->belongsToMany('App\Models\Payment');
    }

}