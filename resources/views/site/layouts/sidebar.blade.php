<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image"><img src="{{asset('storage/uploads/users').'/'.Auth::guard('members')->user()->image}}" class="img-circle" alt=""></div>
      <div class="pull-left info">
        <h3>مرحباً</h3>
        <p>{{Auth::guard('members')->user()->guardian_name}}</p>
        <a href="{{route('site.profile')}}">الحالة <i class="fa fa-circle-o text-success"></i> أونلاين</a>
      </div>
    </div>
    <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='site.home') active @endif">
        <a href="{{route('site.home')}}">
          <i class="fa fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='site.students') active @elseif(Route::currentRouteName()=='site.students.add') active @endif">
        <a href="#">
          <i class="fas fa-graduation-cap"></i>
          <span>الطلاب</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('site.students')}}"><i class="fas fa-bullseye"></i> عرض الطلاب</a></li>
          <li><a href="{{route('site.students.add')}}"><i class="fas fa-bullseye"></i> اضافة طالب</a></li>
        </ul>
      </li>
    </ul>
    <div class="footer-widget">
      <div class="progress transparent progress-small no-radius no-margin">
        <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" style="width: 79%;"></div>
      </div>
      <div class="pull-right">
        <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number">86</span>% </div>
        <a href="{{route('site.lock')}}"><i class="fa fa-power-off"></i></a></div>
    </div>


  </section>
  <!-- Sidebar End -->
</aside>