



@extends('admin.layouts.master')
@section('title')
حفاظ
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper pd">
    <section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>لوحة التحكم</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li class="active">لوحة التحكم</li>
      </ol>
    </section>

    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="small-box box color-green">
          <div class="icon"><i class="fas fa-file-alt"></i></div>
          <div class="box-header">
            <h3 class="box-title">الشقق</h3>

          </div>
          <div class="widget-stats">
            <div class="wrap transparents">
              <a href="{{route('admin.apartment')}}" class="item-title">بيانات <span>الشقق</span></a>
            </div>
          </div>
          <div class="widget-stats">
            <div class="wrap last">
              <a href="{{route('admin.apartments.add')}}" class="item-title">اضافة<span>شقه</span></a>
            </div>
          </div>
          <div class="progress dd">
            <div class="progress-bar" style="width: 50%"></div>
          </div>
        </div>

      </div>



        <div class="col-md-3 col-sm-6">
          <div class="small-box box color-red">
            <div class="icon"><i class="fas fa-file-alt"></i></div>
            <div class="box-header">
              <h3 class="box-title">المصارفات</h3>
            </div>
            <div class="widget-stats">
              <div class="wrap transparents">
                <a href="{{route('admin.expensess')}}" class="item-title">بيانات <span>المصاريف</span></a>
              </div>
            </div>
            <div class="widget-stats">
              <div class="wrap last">
                <a href="{{route('admin.expenses.add')}}" class="item-title">اضافة <span>مصروف</span></a>
              </div>
            </div>
            <div class="progress dd">
              <div class="progress-bar" style="width: 50%"></div>
            </div>
          </div>
        </div>




      <div class="col-md-3 col-sm-6">
        <div class="small-box box color-purple">
          <div class="icon"><i class="fas fa-file-alt"></i></div>
          <div class="box-header">
            <h3 class="box-title">المدفوعات</h3>
          </div>
          <div class="widget-stats">
            <div class="wrap transparents">
              <a href="{{route('admin.payments')}}" class="item-title">بيانات <span>المدفوعات</span></a>
            </div>
          </div>
          <div class="widget-stats">
            <div class="wrap last">
              <a href="{{route('admin.payments.add')}}" class="item-title">اضافة <span>دفع</span></a>
            </div>
          </div>
          <div class="progress dd">
            <div class="progress-bar" style="width: 50%"></div>
          </div>
        </div>
      </div>





      </div>







    </div>
@endsection
  <!-- Content page End -->

@section('footer')

@endsection