@extends('admin.layouts.master')

@inject('model','App\Models\Payment')


@section('title')
اضافه الدفع
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الدفع</small>
      </h1>


      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.payments')}}"> بيانات  الدفع  </a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">اضافه بيانات الدفع </span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">


						{!!Form::model($model,[
                    'action'=>'Admin\PaymentController@store',
                    'files'=>true,
                    'class'=>'m-form m-form--fit m-form--label-align-right'
                    ]) !!}





						<div class="row form-row">
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label">نوع المصروف </label>
									<input name="type" class="form-control" type="text" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label"> الحاله</label>

									<input name="status" class="form-control" type="text" >
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label"> المبلغ المستحق</label>

									<input name="amount_paid" class="form-control" type="text" >
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label"> التفاصيل</label>

									<input name="details" class="form-control" type="text" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label>نوع الدفع</label>
									<select name="payment_type" class="form-control pmd-select2 select2">
										<option></option>
										<option value="دفع شهري">دفع شهري</option>
										<option value="دفع طارءق">دفع طارءق</option>
									</select>
								</div>
							</div>



							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label>السنة</label>
									<select name="year" class="form-control pmd-select2 select2">
										<option></option>
										<option value="2019/2020">2019/2020</option>
										<option value="2020/2021">2020/2021</option>
										<option value="2021/2022">2021/2022</option>
										<option value="2022/2023">2022/2023</option>
										<option value="2023/2024">2023/2024</option>
										<option value="2024/2025">2024/2025</option>
										<option value="2025/2026">2025/2026</option>
										<option value="2026/2027">2026/2027</option>
										<option value="2027/2028">2027/2028</option>
										<option value="2028/2029">2028/2029</option>
										<option value="2029/2030">2029/2030</option>
										<option value="2030/2031">2030/2031</option>
										<option value="2031/2032">2031/2032</option>
										<option value="2032/2033">2032/2033</option>
										<option value="2033/2034">2033/2034</option>
										<option value="2034/2035">2034/2035</option>
										<option value="2035/2036">2035/2036</option>
										<option value="2036/2037">2036/2037</option>
										<option value="2037/2038">2037/2038</option>
										<option value="2038/2039">2038/2039</option>
										<option value="2039/2040">2039/2040</option>
										<option value="2040/2041">2040/2041</option>
									</select>
								</div>
							</div>





							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label>الشهر</label>
									<select name="month" class="form-control pmd-select2 select2">
										<option></option>
										<option value="01">January</option>
										<option value="02">February</option>
										<option value="03">March</option>
										<option value="04">April</option>
										<option value="05">May</option>
										<option value="06">June</option>
										<option value="07">July</option>
										<option value="08">August</option>
										<option value="09">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</select>
								</div>
							</div>
							
							
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label>الشقة</label>
									<select name="apartment" class="form-control pmd-select2 select2">
										<option value="0">الجميع</option>
										@foreach($apartments as $apartment)
										<option value="{{$apartment->number}}">{{$apartment->number}}</option>
                               			 @endforeach
									</select>
								</div>
							</div>











							<div class="col-md-12">
								<br> <a href="{{route('admin.payments')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
								<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
							</div>
						</div>






						{!! Form::close()!!}

					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection