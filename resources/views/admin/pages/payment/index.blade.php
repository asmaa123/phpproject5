@extends('admin.layouts.master')
@section('title')
    الدفوعات
@endsection
@section('content')


    <!-- Content page Start -->
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">الدفوعات</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="tables" class="display">
                                <thead>
                                <tr>
                                    <th class="num">#</th>
                                    <th> نوع المصروف</th>
									<th> الشقة</th>
                                    <th>الحاله </th>
                                    <th> نوع الدفع</th>

                                    <th>الشهر</th>
                                    <th>السنه</th>
                                    <th>المبلغ المستحق</th>
                                    <th>التفاصيل</th>
                                    <th>الوقت</th>

                                    <th>العمليات</th>
                                </tr>


                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td class="num">{{$loop->index + 1}}</td>
                                        <td>{{$payment->type}}</td>
										<td>{{ $payment->apartment === "0" ? "الجميع" : $payment->apartment }}</td>
                                        <td>{{$payment->status}}</td>
                                        <td>{{$payment->payment_type}}</td>
                                        <td>{{$payment->month}}</td>
                                        <td>{{$payment->year}}</td>
                                        <td>{{$payment->amount_paid}}</td>
                                        <td>{{$payment->details}}</td>
                                        <td>{{$payment->created_at}}</td>
                                        <td class="action">
                                            <a href="{{ route('admin.payments.edit' , ['id' => $payment->id]) }}" title="تعديل"> <i class="fa fa-edit"></i>   </a>


                                            <a class=" btndelet" href="{{ route('admin.payments.delete' , ['id' => $payment->id]) }}" title="حذف" >
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <a href="{{ route('admin.payments.show' , ['id' => $payment->id]) }}" title="التفاصيل">  <i class="fa fa-calculator"></i>  </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content page End -->
@endsection

