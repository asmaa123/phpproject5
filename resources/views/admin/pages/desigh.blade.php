@extends('admin.layouts.master')
@section('title')
    صلاحيات المستخدمين
@endsection
@section('content')
    <!-- Content page Start -->
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">صلاحيات  المستخدمين</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">

                            <form class="mtb-15" action="{{route('admin.expenses.create')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}


                                <div class="row">

                                <div class="col-lg-6">
                                    <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                        <label class="control-label">الاسم </label>
                                        <input name="username" class="form-control" type="text">
                                    </div>
                                </div>



                            </div>
                            <br>

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">الايميل </label>
                                            <input name="email" class="form-control" type="text">
                                        </div>
                                    </div>



                                </div>

                                <br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                        <label class="control-label">الرقم السري</label>
                                        <input name="password" class="form-control" type="text">
                                    </div>
                                </div>

                            </div>

                            <br>



                            <div class="row">
                                <div class="col-lg-6">
                                    <select class="form-control" name="rolelist">


                                        <option>   رتبه المستخدم   </option>
                                        @foreach( \App\Role::all() as  $role )
                                            <option value="{{$role->id}}">

                                                {{$role->name }}

                                            </option>
                                        @endforeach


                                    </select>

                                </div>



                            </div>


                            <br>


                            <div class="row">

                                @foreach( \App\Permission::all() as  $permission )


                                <div class="col-sm-3">
                            <div class="checkbox">
                                <label>



                                    <input type="checkbox" name="permission[]" value="{{$permission ->id}}" checked>
                                    {{$permission->name}}

                                </label>
                            </div>






</div>
  @endforeach
 </div>


<br>
                                <div class="row">
                                    <div class=" col-md-7">
                                        <button type="submit" class="btn green addButton">حفظ</button>

                                    </div>
                                </div>



</form>

                        </div>
                            <br><br>

                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <!-- Content page End -->
@endsection

