@extends('admin.layouts.master')
@section('title')
    تقارير بالشقق
@endsection
@section('content')
    <!-- Content page Start -->
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">االشقق  تقارير</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="tables" class="display">
                                <thead>
                                <tr>
                                    <th class="num">#</th>
                                    <th>رقم الشقه</th>
                                    <th> الدور</th>
                                    <th> المبلغ المستحق</th>
                                    <th>صاحب الشقه</th>
                                    <th>الوقت</th>

                                </tr>

                                <tr class="tr-head">
                                    <th>الترتيب</th>
                                    <th>رقم الشقه</th>
                                    <th> الدور</th>
                                    <th> المبلغ المستحق</th>
                                    <th>صاحب الشقه</th>
                                    <th>الوقت</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td class="num">{{$loop->index + 1}}</td>
                                        <td>{{$payment->number}}</td>
                                        <td>{{$payment->floor}}</td>

                                        <td>{{isset($payment->payments()->first()->amount_paid) ?  $payment->payments()->first()->amount_paid :''}}</td>
                                        <td>{{$payment->owner}}</td>
                                        <td>{{$payment->created_at}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content page End -->
@endsection

