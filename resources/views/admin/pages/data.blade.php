@extends('admin.layouts.master')
@section('title')
 ملفات المؤسسة
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
  <section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>ملفات المؤسسة</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.home')}}"> بيانات المؤسسة</a></li>
        <li class="active">ملفات المؤسسة</li>
      </ol>
    </section>
    <section class="content">
            <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <form class="mtb-15" action="{{route('admin.doc.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">
                {{ csrf_field() }}
                  <div class="col-md-5">
                  <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">ادراج ملفات المؤسسة</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                      <div class="box-body">
                      <div class="form-group pmd-textfield pmd-textfield-floating-label">
                        <label class="control-label">نوع الوثيقة</label>
                          <select name="type" class="form-control pmd-select2 select2">
                                <option>          </option>
                                @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                          </select>
                        </div><!-- /.form-group -->
                        <div class="form-group custom-inputfile">
                                    <input type="file" name="image" id="file-7" class="inputfile inputfile-6"/>
                                    <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>  حمل ملف </strong></label>
                                    <input type="hidden" value="org" name="storage2">
                                </div>
                      </div>
                      <div class="box-footer">
                        <a href="{{route('admin.home')}}" class="btn btn-orange pmd-ripple-effect btn-sm">  الغاء</a>
                        <button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">  حفظ</button>
                      </div>
                    </div>
                  </div><!-- End col -->
              </form>

              <div class="col-md-7">
              <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold"> ملفات المؤسسة</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                  <div class="box-body white-bg">
                    <table class="table table-striped">
                      <tbody>
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>الملف</th>
                        <th>العمليات</th>
                      </tr>
                      @foreach($docs as $doc)
                      <tr>
                        <td>{{$loop->index + 1}}.</td>
                        <td><a href="{{asset('storage/uploads/org').'/'.$doc->file}}" target="_blank"><i class="fa fa-file-text"></i>   {{$doc->name}}</a></td>
                        <td>
                          <a href="{{url('admin/downloadFile/'.$doc->id)}}" target="_blank" title="تحميل"> <i class="fa fa-download"></i></a>
                          <a type="submit" class="btndelet" href="{{ route('admin.doc.delete' , ['id' => $doc->id]) }}" title="حذف"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody></table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- End col -->
              </div><!-- End col -->
            </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection