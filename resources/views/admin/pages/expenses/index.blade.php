@extends('admin.layouts.master')
@section('title')
   المصاريف
@endsection
@section('content')
    <!-- Content page Start -->
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">المصاريف</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="tables" class="display">
                                <thead>
                                <tr>
                                    <th class="num">#</th>
                                    <th>الاسم  </th>
                                    <th> مبلغ الصرف</th>
                                    <th> مبلغ الدفع</th>
                                    <th> التفاصيل</th>
                                    <th>الوقت</th>

                                    <th>العمليات</th>
                                </thead>
                                <tbody>

                                @foreach($expenses as $expense)
                                    <tr>
                                        <td class="num">{{$loop->index + 1}}</td>
                                        <td>{{$expense->name}}</td>
                                        <td>{{$expense->caching}}</td>
                                        <td>{{$expense->pay}}</td>
                                        <td>{{$expense->details}}</td>
                                        <td>{{$expense->created_at}}</td>
                                        <td class="action">

                                          <a href="{{ route('admin.expenses.edit' , ['id' => $expense->id]) }}" title="تعديل"> <i class="fa fa-edit"></i>   </a>
                                            <a class=" btndelet" href="{{ route('admin.expenses.delete' , ['id' => $expense->id]) }}" title="حذف" >
                                                <i class="fa fa-trash"></i>
                                            </a>







                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content page End -->
@endsection

