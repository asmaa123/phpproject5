@extends('admin.layouts.master') 
@section('title')
تعديل المصروف
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات المصروف</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.expensess')}}"> بيانات  المصروف  </a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات االمصروف </span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">


						{!!Form::model($expense,[
                    'action'=>['Admin\ExpenceController@postEdit',$expense->id],
                    'method'=>'put',

                    'class'=>'m-form m-form--fit m-form--label-align-right'
                    ]) !!}





						<div class="row form-row">
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label">   مين الي صرف</label>
									<input name="name" class="form-control" type="text" value="{{$expense->name}}">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label">التفاصيل </label>

									<input name="details" class="form-control" type="text" value="{{$expense->details}}">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label">   صرف كام  </label>


									<input name="caching" class="form-control" type="text" value="{{$expense->caching}}">									</div>
							</div>

							<div class="col-md-6">
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<label class="control-label"> دفع كام</label>
									<input name="pay" class="form-control" type="text" value="{{$expense->pay}}">
								</div>
							</div>




							<div class="col-md-12">
								<br> <a href="{{route('admin.expensess')}}" class="btn btn-primary btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
								<button type="submit" class="btn btn-blue btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
							</div>
						</div>






						{!! Form::close()!!}

					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection