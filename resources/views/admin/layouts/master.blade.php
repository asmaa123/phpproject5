<!DOCTYPE html>
<html lang="ar" dir="rtl">
    <head>
        <!-- Meta Tags
        ========================== -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for form layouts" name="description" />
        <meta content="" name="author" />
        <meta name="csrf_token" content="{{csrf_token()}}">


        <!-- Site Title
        ========================== -->
        <title>@yield('title')</title>
        
        <!-- Favicon
        ===========================-->
        <link rel="shortcut icon" href="{{asset('assets/admin/img/logo-mini.png')}}">

        
        <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


        <link rel="stylesheet" href="{{asset('assets/admin/css/datatables.css')}}">
        

        <link rel="stylesheet" href="{{asset('assets/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="{{asset('assets/admin/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{asset('assets/admin/plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{asset('assets/admin/css/datetimepicker.css')}}">

        <!-- Select2 
        <link rel="stylesheet" href="{{asset('assets/admin/css/select2.css')}}">-->  

        

        
        <link href="{{asset('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet">
        <link href="{{asset('assets/admin/css/sweetalert.css')}}" rel="stylesheet">
        <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/admin/css/textfield.css')}}">
        <link rel="stylesheet" href="{{asset('assets/admin/css/component.css')}}">

        <link rel="stylesheet" href="{{asset('assets/admin/css/style.css')}}" id="stylesheet">
        
        

        

        

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="sidebar-mini">
    
        <div class="wrapper">
            @include('admin.layouts.header')
            @include('admin.layouts.sidebar')
            @yield('content')
        </div>

        @yield('modals')
        @yield('templates')

        <!-- common edit modal with ajax for all project -->
        <div id="common-modal" class="modal fade" role="dialog">
                    <!-- modal -->
        </div>

        <!-- delete with ajax for all project -->
        <div id="delete-modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                    </div>
        </div>
        <script id="template-modal" type="text/html" >
                    <div class = "modal-content" >
                        <input type = "hidden" name = "_token" value="{{ csrf_token() }}" >
                        <div class = "modal-header" >
                            <button type = "button" class = "close" data - dismiss = "modal" > &times; </button>
                            <h4 class = "modal-title bold" >هل تريد مسح العنصر ؟</h4>
                        </div>
                        <div class = "modal-body" >
                            <p >يرجى العلم بأنه سيتم حذف العنصر بكل ما يتعلق به من بيانات</p>
                        </div>
                        <div class = "modal-footer" >
                            <a
                                href = "{url}"
                                id = "delete" class = "btn btn-danger" >
                                <li class = "fa fa-trash" > </li> مسح
                            </a>

                            <button type = "button" class = "btn btn-default" data-dismiss = "modal" >
                                <li class = "fa fa-times" > </li> أغلق</button >
                        </div>
                    </div>
        </script>
                
        @include('admin.templates.alerts')
        @include('admin.templates.delete-modal')

        <form action="#" id="csrf">{!! csrf_field() !!}</form>

        <!-- Scripts
            ========================== -->
        @if(    Route::currentRouteName() == 'admin.teachers.edit' || Route::currentRouteName() == 'admin.students.edit' ||  Route::currentRouteName() == 'admin.teachers.add' || Route::currentRouteName() == 'admin.levels.add' || Route::currentRouteName() == 'admin.guardians.edit' )
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        
        
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
            $(function () {
              function readURL(input) {
                  if (input.files && input.files[0]) {
                      var reader = new FileReader();
                      reader.onload = function(e) {
                          $('#blah').attr('src', e.target.result);
                      }
                      reader.readAsDataURL(input.files[0]);
                  }
              }
              $("#imgInp").change(function() {
                  readURL(this);
              });
              
              function readURL(input) {
                  if (input.files && input.files[0]) {
                      var reader = new FileReader();
                      reader.onload = function(e) {
                          $('#blah2').attr('src', e.target.result);
                      }
                      reader.readAsDataURL(input.files[0]);
                  }
              }
              $("#imgInp2").change(function() {
                  readURL(this);
              });
              /* Default date and time picker */
              $('.datetimepicker-default').datetimepicker({
              });
              $('.datepicker').datetimepicker({
                'format' : "YYYY-MM-DD",
              });
              $('.timepicker').datetimepicker({
                'format' : "LT",
              });
            });
          </script>


        
        <script src="{{asset('assets/admin/js/tab-scrollable.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

          $(document).ready(function() {
            $('#advanced-check').change(function() {
              $('#advanced-search').toggle();
            });
          });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>


        @elseif(Route::currentRouteName() == 'admin.home') 
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        
        
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
          $(function () {
            /* Default date and time picker */
            $('.datetimepicker-default').datetimepicker({
            });
            $('.datepicker').datetimepicker({
              'format' : "YYYY-MM-DD",
            });
            $('.timepicker').datetimepicker({
              'format' : "LT",
            });
          });
        </script>


        
        <script src="{{asset('assets/admin/js/tab-scrollable.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

        $(document).ready(function() {
          $('#advanced-check').change(function() {
            $('#advanced-search').toggle();
          });
        });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script> 









        @elseif( Route::currentRouteName() == 'admin.apartment' ||  Route::currentRouteName() == 'admin.reports.payments' || Route::currentRouteName() == 'admin.payments'  || Route::currentRouteName() == 'admin.payments.show'  || Route::currentRouteName() == 'admin.expensess'  || Route::currentRouteName() == 'admin.reports.passlevels'  ||  Route::currentRouteName() == 'admin.reportexpense' || Route::currentRouteName() == 'admin.currentbalance'  || Route::currentRouteName() == 'admin.reports.parts'  || Route::currentRouteName() == 'admin.reports.levelstudents'  || Route::currentRouteName() == 'admin.reports.studentdatacourse' ||  Route::currentRouteName() == 'admin.reports.studentss'  ||  Route::currentRouteName() == 'admin.reports.Exemptstudent'  || Route::currentRouteName() == 'admin.reports.teacher'  || Route::currentRouteName() == 'admin.reports.studentcourse'  || Route::currentRouteName() == 'admin.reports.datacenter' || Route::currentRouteName() == 'admin.reports.students'  ||Route::currentRouteName() == 'admin.reports.coursebookstudent' ||Route::currentRouteName() == 'admin.reports.coursereservation'||Route::currentRouteName() == 'admin.reports.financialclaims' || Route::currentRouteName() == 'admin.reports.Scounts' || Route::currentRouteName() == 'admin.students.pendingadds' || Route::currentRouteName() == 'admin.students.pendingedits' || Route::currentRouteName() == 'admin.types' || Route::currentRouteName() == 'admin.towns' || Route::currentRouteName() == 'admin.seasons' || Route::currentRouteName() == 'admin.store' || Route::currentRouteName() == 'admin.reports.salaries' || Route::currentRouteName() == 'admin.jobs' || Route::currentRouteName() == 'admin.reports.attend' || Route::currentRouteName() == 'admin.reports.absent' || Route::currentRouteName() == 'admin.reports.counts' || Route::currentRouteName() == 'admin.reports.grades' || Route::currentRouteName() == 'admin.students' || Route::currentRouteName() == 'admin.students.count' || Route::currentRouteName() == 'admin.teachers' || Route::currentRouteName() == 'admin.guardians' || Route::currentRouteName() == 'admin.materials' || Route::currentRouteName() == 'admin.centers' || Route::currentRouteName() == 'admin.courses' ||  Route::currentRouteName() == 'admin.transportations')
        <script src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
        
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <script src="https://nightly.datatables.net/responsive/js/dataTables.responsive.min.js"></script>

        <script>
            $(document).ready(function() {
              var t = $('#tables').DataTable( {
                  initComplete: function () {
                      this.api().columns().every( function () {
                          var column = this;
                          var select = $('<select><option value=""></option></select>')
                              .appendTo( $(column.footer()).empty() )
                              .on( 'change', function () {
                                  var val = $.fn.dataTable.util.escapeRegex(
                                      $(this).val()
                                  );
          
                                  column
                                      .search( val ? '^'+val+'$' : '', true, false )
                                      .draw();
                              } );
          
                          column.data().unique().sort().each( function ( d, j ) {
                              select.append( '<option value="'+d+'">'+d+'</option>' )
                          } );
                      } );
                  },
                  "language":{
                    "decimal":        "",
                    "emptyTable":     "لا يوجد بيانات",
                    "info": "عرض صفحة _PAGE_ من _PAGES_ صفحات",
                    "infoEmpty":      "عرض مدخلات من 0 الى 0 ",
                    "infoFiltered":   "(محدد من _MAX_ عنصر)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "عرض مدخلات القائمة",
                    "loadingRecords": "...تحميل",
                    "processing":     "...تنفيذ",
                    "search":         "ابحث:",
                    "zeroRecords":    "لا يوجد نتائج للبحث",
                    "paginate": {
                        "first":      "الأول",
                        "last":       "الأخير",
                        "next":       "التالى",
                        "previous":   "السابق"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                  },
                  responsive: true,
                  dom: 'Bfrtip',
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'noVis'
                        }
                    ],
                    buttons: [
                        {
                            text: 'نسخ',
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [ ':visible' ]
                            }
                        },
                        {
                          text: 'اكسل',
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                          text: 'طباعة',
                            extend: 'print',
                            messageTop: '',
                            exportOptions: {
                                columns: [ ':visible' ]
                            }
                        },
                        {
                          extend: 'colvis',
                          className: 'btn-orange',
                          text: 'تحديد الأعمدة'
                        },
                        
                    ],
                    orderCellsTop: true,
                    fixedHeader: true,
                    pageLength: 100,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                } );
            
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

                $('#tables thead tr:eq(1) th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="بحث ب'+title+'" class="form-control" />' );
                } );
            
            // Apply the search
                $( '#tables thead'  ).on( 'keyup', ".form-control",function () {
                    t.column( $(this).parent().index() )
                        .search( this.value )
                        .draw();
                } );
            } );
        </script>
        
        

        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        


        
        
        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>

        
        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>

      @else
        <script data-require="jquery@*" data-semver="2.1.4" src="{{asset('assets/admin/js/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/global.js')}}"></script>

        <script src="{{asset('assets/admin/js/textfield.js')}}"></script>
        <!-- Select2 
        <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>-->

        
        <!-- fullscreen -->
        <script src="{{asset('assets/admin/js/screenfull.js')}}"></script>

        <!-- text-rotator -->
        <script src="{{asset('assets/admin/js/morphext.js')}}"></script>

        <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
        <script>
          $(function () {
            /* Default date and time picker */
            $('.datepicker').datetimepicker({
              'format' : "YYYY-MM-DD",
            });
            
            $('.datetimepicker-default').datetimepicker({
              'format' : "LT",
            });

            
          });
        </script>
        

        
        <script src="{{asset('assets/admin/js/custom-file-input.js')}}"></script>

        <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
        
        <script>

        $(document).ready(function() {
          $('#advanced-check').change(function() {
            $('#advanced-search').toggle();
          });
        });  

        </script>


        

        <script src="{{asset('assets/admin/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script type="text/javascript">
          /* Nice Scroll
          ===============================*/
          $(document).ready(function () {
              
              "use strict";
              
              $("html").niceScroll({
                  scrollspeed: 60,
                  mousescrollstep: 35,
                  cursorwidth: 5,
                  cursorcolor: 'rgba(243, 131, 78, 0.7)',
                  cursorborder: 'none',
                  background: 'rgba(27, 30, 36, 0.0)',
                  cursorborderradius: 3,
                  autohidemode: false,
                  cursoropacitymin: 0.1,
                  cursoropacitymax: 1,
                  zindex: "999",
                  horizrailenabled: false
              });
            
          });
        </script>
        <script src="{{asset('assets/admin/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-sweetalert/sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/ui-sweetalert.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('assets/admin/js/process.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/main.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/main2.js')}}" type="text/javascript"></script>
        

        
        @endif
        @yield('footer')
    </body>
</html>