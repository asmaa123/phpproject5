<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image"><img src="{{asset('storage/uploads/users').'/'.Auth::guard('admins')->user()->image}}" class="img-circle" alt=""></div>
      <div class="pull-left info">
        <h3>مرحباً</h3>
        <p>{{Auth::guard('admins')->user()->name}}</p>
        <a href="{{route('admin.profile')}}">الحالة <i class="fas fa-bullseye text-success"></i> أونلاين</a>
      </div>
    </div>
    @if(Auth::guard('admins')->user()->type == 'student' || Auth::guard('admins')->user()->type == 'admin' )







      <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>



        <li class="treeview @if(Route::currentRouteName()=='admin.apartment') active @elseif(Route::currentRouteName()=='admin.apartments.add') active @endif">
        <a href="#">
          <i class="fas fa-book-reader"></i>
          <span> الشقق</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.apartment')}}"><i class="fas fa-bullseye"></i> عرض  الشقق</a></li>
          <li><a href="{{route('admin.apartments.add')}}"><i class="fas fa-bullseye"></i> اضافة  الشقه</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.towns') active @elseif(Route::currentRouteName()=='admin.towns.add') active @endif">
        <a href="#">
          <i class="fas fa-globe"></i>
          <span>المدفوعات </span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.payments')}}"><i class="fas fa-bullseye"></i> عرض الدفعات </a></li>
          <li><a href="{{route('admin.payments.add')}}"><i class="fas fa-bullseye"></i> اضافة الدفع</a></li>
        </ul>
      </li>

        <li class="treeview @if(Route::currentRouteName()=='admin.expensess') active @elseif(Route::currentRouteName()=='admin.expenses.add') active @endif">
          <a href="#">
            <i class="fas fa-newspaper"></i>
            <span> المصاريف</span>
            <i class="fas fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('admin.expensess')}}"><i class="fas fa-bullseye"></i> عرض  المصاريف</a></li>
            <li><a href="{{route('admin.expenses.add')}}"><i class="fas fa-bullseye"></i> اضافة  المصاريف</a></li>
          </ul>
        </li>



      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.payments')}}"><i class="fas fa-bullseye"></i> تقارير بالشقق برصيد مستحق</a></li>
          <li><a href="{{route('admin.reportexpense')}}"><i class="fas fa-bullseye"></i> تقارير بالمصاريف</a></li>
          <li><a href="{{route('admin.currentbalance')}}"><i class="fas fa-bullseye"></i> تقارير الرصيد الحالي</a></li>
        </ul>
      </li>
      
    </ul>
    @elseif(Auth::guard('admins')->user()->type == 'teacher')
    <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.attend')}}"><i class="fas fa-bullseye"></i> تقارير حضور الموظفين</a></li>
        </ul>
      </li>
    </ul>
    @elseif(Auth::guard('admins')->user()->type == 'accountant')
    <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.org') active @endif">
        <a href="{{route('admin.org')}}">
          <i class="fas fa-edit"></i>
          <span>بيانات المؤسسة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.data') active @endif">
        <a href="{{route('admin.data')}}">
          <i class="fas fa-newspaper"></i>
          <span>وثائق المؤسسة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.store') active @endif">
        <a href="{{route('admin.store')}}">
          <i class="fas fa-shopping-cart"></i>
          <span> الخزينة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.student.store') active @endif">
        <a href="{{route('admin.student.store')}}">
          <i class="fas fa-shopping-cart"></i>
          <span> تسجيل بيانات الطلاب</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.absent')}}"><i class="fas fa-bullseye"></i> تقارير حضور الطلاب</a></li>
          <li><a href="{{route('admin.reports.grades')}}"><i class="fas fa-bullseye"></i> تقارير درجات الطلاب</a></li>
          <li><a href="{{route('admin.reports.counts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب</a></li>
          <li><a href="{{route('admin.reports.Scounts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب بايجاز</a></li>
          <li><a href="{{route('admin.reports.attend')}}"><i class="fas fa-bullseye"></i> تقارير حضور الموظفين</a></li>
          <li><a href="{{route('admin.reports.salaries')}}"><i class="fas fa-bullseye"></i> تقارير مرتبات الموظفين</a></li>
        </ul>
      </li>
      
    </ul>
    @endif
  </section>
  <!-- Sidebar End -->
</aside>