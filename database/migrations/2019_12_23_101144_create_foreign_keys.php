<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('apartment_payment', function(Blueprint $table) {
			$table->foreign('payment_id')->references('id')->on('payments')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('apartment_payment', function(Blueprint $table) {
			$table->foreign('partment_id')->references('id')->on('payments')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('apartment_payment', function(Blueprint $table) {
			$table->dropForeign('apartment_payment_payment_id_foreign');
		});
		Schema::table('apartment_payment', function(Blueprint $table) {
			$table->dropForeign('apartment_payment_partment_id_foreign');
		});
	}
}