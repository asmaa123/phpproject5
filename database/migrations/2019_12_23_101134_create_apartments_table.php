<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApartmentsTable extends Migration {

	public function up()
	{
		Schema::create('apartments', function(Blueprint $table) {
			$table->increments('id');
			$table->string('number');
			$table->string('floor');
			$table->string('phone');
			$table->string('owner');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('apartments');
	}
}