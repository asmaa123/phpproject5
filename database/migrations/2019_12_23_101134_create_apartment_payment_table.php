<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApartmentPaymentTable extends Migration {

	public function up()
	{
		Schema::create('apartment_payment', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('payment_id')->unsigned();
			$table->integer('apartment_id')->unsigned();
			$table->string('status');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('apartment_payment');
	}
}