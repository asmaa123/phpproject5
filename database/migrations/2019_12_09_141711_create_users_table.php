<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('username', 65535)->nullable();
			$table->string('type', 50)->nullable();


			$table->string('email')->nullable()->unique();
			$table->string('password')->nullable();
			$table->text('recover', 65535)->nullable();
			$table->string('country')->nullable();

			$table->text('phone', 65535)->nullable();
			$table->string('address')->nullable();

			$table->string('image')->nullable();
			$table->text('remember_token', 65535)->nullable();
			$table->boolean('active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
