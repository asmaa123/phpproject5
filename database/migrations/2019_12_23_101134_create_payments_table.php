<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	public function up()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->increments('id');
			$table->string('type')->nullable();
			$table->string('payment_type')->nullable();
			$table->string('amount_paid')->nullable();
			$table->string('status')->nullable();
            $table->string('details')->nullable();
            $table->string('apartment');
			$table->string('year');
			$table->string('month');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('payments');
	}
}