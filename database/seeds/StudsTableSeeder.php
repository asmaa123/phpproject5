<?php

use Illuminate\Database\Seeder;

class StudsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('studs')->delete();
        
        \DB::table('studs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'asmaa',
                'center' => 'laka',
                'course' => 'a1',
                'birth' => 'asd',
                'national_id' => '23345',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:24:52',
            ),
        ));
        
        
    }
}