<?php

use Illuminate\Database\Seeder;

class CourseMaterialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('course_materials')->delete();
        
        \DB::table('course_materials')->insert(array (
            0 => 
            array (
                'id' => 1,
                'course_id' => 1,
                'material_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:15:30',
            ),
        ));
        
        
    }
}