<?php

use Illuminate\Database\Seeder;

class LevelMaterialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('level_materials')->delete();
        
        \DB::table('level_materials')->insert(array (
            0 => 
            array (
                'id' => 1,
                'level_id' => 1,
                'material_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:17:15',
            ),
        ));
        
        
    }
}