<?php

use Illuminate\Database\Seeder;

class CenterDocumentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('center_documents')->delete();
        
        \DB::table('center_documents')->insert(array (
            0 => 
            array (
                'id' => 19,
                'center_id' => 2,
            'file' => 'laimannung-GLrm3MJXxcI-unsplash (1).jpg',
                'type' => 'ثث',
                'created_at' => '2019-12-04 13:01:02',
                'updated_at' => '2019-12-04 13:01:02',
            ),
            1 => 
            array (
                'id' => 15,
                'center_id' => 8,
                'file' => 'asmaaabozied.pdf',
                'type' => 'التليجرام',
                'created_at' => '2019-11-11 11:28:58',
                'updated_at' => '2019-11-11 11:28:58',
            ),
            2 => 
            array (
                'id' => 17,
                'center_id' => 11,
                'file' => 'screenshot-trial-hofaz.lubic-kw.com-2019.11.png',
                'type' => 'شعار',
                'created_at' => '2019-11-19 10:57:37',
                'updated_at' => '2019-11-19 10:57:37',
            ),
            3 => 
            array (
                'id' => 14,
                'center_id' => 3,
                'file' => 'img289.jpg',
                'type' => 'english',
                'created_at' => '2019-11-11 11:20:05',
                'updated_at' => '2019-11-11 11:20:05',
            ),
            4 => 
            array (
                'id' => 6,
                'center_id' => 5,
                'file' => 'module_table_bottom.png',
                'type' => '1',
                'created_at' => '2019-11-06 08:42:37',
                'updated_at' => '2019-11-06 08:42:37',
            ),
            5 => 
            array (
                'id' => 11,
                'center_id' => 5,
                'file' => 'img289.jpg',
                'type' => 'english',
                'created_at' => '2019-11-07 15:41:59',
                'updated_at' => '2019-11-07 15:41:59',
            ),
            6 => 
            array (
                'id' => 8,
                'center_id' => 5,
                'file' => 'img289.jpg',
                'type' => 'english',
                'created_at' => '2019-11-06 13:06:54',
                'updated_at' => '2019-11-06 13:06:54',
            ),
            7 => 
            array (
                'id' => 9,
                'center_id' => 5,
                'file' => 'img289.jpg',
                'type' => 'english',
                'created_at' => '2019-11-06 13:26:15',
                'updated_at' => '2019-11-06 13:26:15',
            ),
            8 => 
            array (
                'id' => 20,
                'center_id' => 2,
            'file' => 'laimannung-GLrm3MJXxcI-unsplash (1).jpg',
                'type' => 'ثث',
                'created_at' => '2019-12-04 13:01:12',
                'updated_at' => '2019-12-04 13:01:12',
            ),
            9 => 
            array (
                'id' => 21,
                'center_id' => 39,
            'file' => 'img289 (1) (1).jpg',
                'type' => 'الاول',
                'created_at' => '2019-12-05 09:30:10',
                'updated_at' => '2019-12-05 09:30:10',
            ),
            10 => 
            array (
                'id' => 22,
                'center_id' => 41,
                'file' => 'psd-covers.jpg',
                'type' => 'استمارة القبول',
                'created_at' => '2019-12-08 07:27:43',
                'updated_at' => '2019-12-08 07:27:43',
            ),
            11 => 
            array (
                'id' => 23,
                'center_id' => 53,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'type' => 'qqq',
                'created_at' => '2019-12-09 09:41:24',
                'updated_at' => '2019-12-09 09:41:24',
            ),
        ));
        
        
    }
}