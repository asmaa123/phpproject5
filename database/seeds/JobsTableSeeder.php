<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jobs')->delete();
        
        \DB::table('jobs')->insert(array (
            0 => 
            array (
                'id' => 3,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 11:57:33',
                'updated_at' => '2019-12-04 11:57:33',
            ),
            1 => 
            array (
                'id' => 1,
                'job_name' => 'developper1',
                'job_type' => 'see',
                'active' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-09 09:54:34',
            ),
            2 => 
            array (
                'id' => 4,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 11:58:11',
                'updated_at' => '2019-12-04 11:58:11',
            ),
            3 => 
            array (
                'id' => 5,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 11:58:47',
                'updated_at' => '2019-12-04 11:58:47',
            ),
            4 => 
            array (
                'id' => 6,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 11:59:59',
                'updated_at' => '2019-12-04 11:59:59',
            ),
            5 => 
            array (
                'id' => 7,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 12:01:09',
                'updated_at' => '2019-12-04 12:01:09',
            ),
            6 => 
            array (
                'id' => 8,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 12:03:06',
                'updated_at' => '2019-12-04 12:03:06',
            ),
            7 => 
            array (
                'id' => 9,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 12:53:53',
                'updated_at' => '2019-12-04 12:53:53',
            ),
            8 => 
            array (
                'id' => 12,
                'job_name' => 'حلقه الاول',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 14:13:45',
                'updated_at' => '2019-12-04 14:13:45',
            ),
            9 => 
            array (
                'id' => 13,
                'job_name' => 'it7777',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 14:14:28',
                'updated_at' => '2019-12-05 10:34:03',
            ),
            10 => 
            array (
                'id' => 15,
                'job_name' => 'محفظ قرآن',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-04 18:23:33',
                'updated_at' => '2019-12-04 18:23:33',
            ),
            11 => 
            array (
                'id' => 18,
                'job_name' => 'الوظيفه الاخيره',
                'job_type' => NULL,
                'active' => 1,
                'created_at' => '2019-12-09 09:54:59',
                'updated_at' => '2019-12-09 09:54:59',
            ),
        ));
        
        
    }
}