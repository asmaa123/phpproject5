<?php

use Illuminate\Database\Seeder;

class CourseTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('course_types')->delete();
        
        \DB::table('course_types')->insert(array (
            0 => 
            array (
                'id' => 65,
                'type_name' => 'حلقه الاول11',
                'active' => 1,
                'created_at' => '2019-12-08 11:40:31',
                'updated_at' => '2019-12-08 11:40:31',
            ),
            1 => 
            array (
                'id' => 63,
                'type_name' => 'asmaa11',
                'active' => 1,
                'created_at' => '2019-12-08 11:24:03',
                'updated_at' => '2019-12-08 11:24:03',
            ),
            2 => 
            array (
                'id' => 64,
                'type_name' => 'asmaa',
                'active' => 1,
                'created_at' => '2019-12-08 11:24:03',
                'updated_at' => '2019-12-08 11:24:03',
            ),
            3 => 
            array (
                'id' => 61,
                'type_name' => 'حلقه العشره',
                'active' => 1,
                'created_at' => '2019-12-08 11:18:53',
                'updated_at' => '2019-12-08 11:18:53',
            ),
            4 => 
            array (
                'id' => 62,
                'type_name' => '7',
                'active' => 1,
                'created_at' => '2019-12-08 11:18:53',
                'updated_at' => '2019-12-08 11:18:53',
            ),
            5 => 
            array (
                'id' => 56,
                'type_name' => 'مبتدئين 08/12/2019',
                'active' => 1,
                'created_at' => '2019-12-08 07:28:44',
                'updated_at' => '2019-12-08 07:28:44',
            ),
            6 => 
            array (
                'id' => 58,
                'type_name' => 'الحلقه الوسطه',
                'active' => 1,
                'created_at' => '2019-12-08 11:18:10',
                'updated_at' => '2019-12-08 11:18:10',
            ),
            7 => 
            array (
                'id' => 59,
                'type_name' => 'الحلقه الاخيره',
                'active' => 1,
                'created_at' => '2019-12-08 11:18:10',
                'updated_at' => '2019-12-08 11:18:10',
            ),
            8 => 
            array (
                'id' => 60,
                'type_name' => '252',
                'active' => 1,
                'created_at' => '2019-12-08 11:18:53',
                'updated_at' => '2019-12-08 11:18:53',
            ),
            9 => 
            array (
                'id' => 55,
                'type_name' => 'الآرتقاء',
                'active' => 1,
                'created_at' => '2019-12-05 18:13:40',
                'updated_at' => '2019-12-05 18:13:40',
            ),
            10 => 
            array (
                'id' => 54,
                'type_name' => 'e4t1111111',
                'active' => 1,
                'created_at' => '2019-12-05 10:26:21',
                'updated_at' => '2019-12-05 10:26:34',
            ),
            11 => 
            array (
                'id' => 52,
                'type_name' => 'المتقدمون',
                'active' => 1,
                'created_at' => '2019-12-04 18:18:49',
                'updated_at' => '2019-12-04 18:18:49',
            ),
            12 => 
            array (
                'id' => 4,
                'type_name' => 'حلقه الاول',
                'active' => 1,
                'created_at' => '2019-12-04 12:16:37',
                'updated_at' => '2019-12-04 12:16:37',
            ),
            13 => 
            array (
                'id' => 50,
                'type_name' => 'حلقه الاول333',
                'active' => 1,
                'created_at' => '2019-12-04 12:42:16',
                'updated_at' => '2019-12-04 12:42:16',
            ),
            14 => 
            array (
                'id' => 48,
                'type_name' => 'حلقه الاول5',
                'active' => 1,
                'created_at' => '2019-12-04 12:24:39',
                'updated_at' => '2019-12-04 12:24:39',
            ),
            15 => 
            array (
                'id' => 46,
                'type_name' => 'حلقه الرابعه',
                'active' => 1,
                'created_at' => '2019-12-04 12:16:35',
                'updated_at' => '2019-12-04 12:16:35',
            ),
            16 => 
            array (
                'id' => 3,
                'type_name' => 'الكتاب',
                'active' => 1,
                'created_at' => '2019-12-03 14:54:19',
                'updated_at' => '2019-12-03 14:54:19',
            ),
            17 => 
            array (
                'id' => 2,
                'type_name' => 'حلقه الخامسه',
                'active' => 1,
                'created_at' => '2019-12-04 09:25:25',
                'updated_at' => '2019-12-04 09:25:25',
            ),
            18 => 
            array (
                'id' => 1,
                'type_name' => 'حلقه الاول3',
                'active' => 1,
                'created_at' => '2019-12-03 14:25:27',
                'updated_at' => '2019-12-03 14:25:27',
            ),
            19 => 
            array (
                'id' => 67,
                'type_name' => 'النوع الاول1',
                'active' => 1,
                'created_at' => '2019-12-09 09:43:38',
                'updated_at' => '2019-12-09 09:44:13',
            ),
        ));
        
        
    }
}