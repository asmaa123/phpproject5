<?php

use Illuminate\Database\Seeder;

class AttendTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('attend')->delete();
        
        \DB::table('attend')->insert(array (
            0 => 
            array (
                'id' => 2,
                'teacher_id' => 1,
                'student_id' => 1,
                'center_id' => 1,
                'date' => '2019-12-09',
                'days' => '20-2-2016',
                'month' => '12',
                'year' => '2020',
                'attends' => '00:00:00',
                'leaves' => '21:24:00',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:14:53',
            ),
            1 => 
            array (
                'id' => 1,
                'teacher_id' => 1,
                'student_id' => 1,
                'center_id' => 1,
                'date' => '2019-11-04',
                'days' => '11deed',
                'month' => 'dcdfr',
                'year' => 'dfdef',
                'attends' => '00:00:01',
                'leaves' => '00:00:02',
                'created_at' => NULL,
                'updated_at' => '2019-11-04 04:37:03',
            ),
        ));
        
        
    }
}