<?php

use Illuminate\Database\Seeder;

class OrgsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('orgs')->delete();
        
        \DB::table('orgs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'logo' => 'sherry-christian-8Myh76_3M2U-unsplash.jpg',
                'name' => 'asma11',
                'address' => 'fgt',
                'business_registration' => 'y',
                'tax_card' => 'yuu',
                'phone' => '010',
                'fax' => 'uu',
                'email' => 'as@as.ac',
                'website' => 'hyyt',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 13:05:54',
            ),
        ));
        
        
    }
}