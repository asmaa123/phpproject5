<?php

use Illuminate\Database\Seeder;

class StudentArchivesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_archives')->delete();
        
        \DB::table('student_archives')->insert(array (
            0 => 
            array (
                'id' => 56,
                'center_id' => 1,
                'level_id' => 53,
                'season_id' => 38,
                'student_id' => 57,
                'year' => '2037/2038',
                'created_at' => '2019-12-09 09:57:16',
                'updated_at' => '2019-12-09 09:57:16',
            ),
            1 => 
            array (
                'id' => 55,
                'center_id' => 1,
                'level_id' => 53,
                'season_id' => 33,
                'student_id' => 56,
                'year' => '2024/2025',
                'created_at' => '2019-12-09 09:33:31',
                'updated_at' => '2019-12-09 09:33:31',
            ),
            2 => 
            array (
                'id' => 54,
                'center_id' => 1,
                'level_id' => 53,
                'season_id' => 29,
                'student_id' => 55,
                'year' => '2037/2038',
                'created_at' => '2019-12-09 09:22:40',
                'updated_at' => '2019-12-09 09:22:40',
            ),
            3 => 
            array (
                'id' => 53,
                'center_id' => 1,
                'level_id' => 53,
                'season_id' => 33,
                'student_id' => 54,
                'year' => '2020/2021',
                'created_at' => '2019-12-09 09:19:41',
                'updated_at' => '2019-12-09 09:19:41',
            ),
            4 => 
            array (
                'id' => 52,
                'center_id' => 41,
                'level_id' => NULL,
                'season_id' => 34,
                'student_id' => 53,
                'year' => '2035/2036',
                'created_at' => '2019-12-08 15:50:53',
                'updated_at' => '2019-12-08 15:50:53',
            ),
            5 => 
            array (
                'id' => 51,
                'center_id' => 37,
                'level_id' => NULL,
                'season_id' => 37,
                'student_id' => 52,
                'year' => '2019/2020',
                'created_at' => '2019-12-08 15:49:55',
                'updated_at' => '2019-12-08 15:49:55',
            ),
            6 => 
            array (
                'id' => 50,
                'center_id' => 1,
                'level_id' => 3,
                'season_id' => 35,
                'student_id' => 51,
                'year' => '2036/2037',
                'created_at' => '2019-12-05 12:04:07',
                'updated_at' => '2019-12-05 12:04:07',
            ),
            7 => 
            array (
                'id' => 49,
                'center_id' => 1,
                'level_id' => 3,
                'season_id' => 35,
                'student_id' => 50,
                'year' => '2035/2036',
                'created_at' => '2019-12-05 11:03:46',
                'updated_at' => '2019-12-05 11:03:46',
            ),
            8 => 
            array (
                'id' => 48,
                'center_id' => 1,
                'level_id' => 3,
                'season_id' => 29,
                'student_id' => 49,
                'year' => '2019/2020',
                'created_at' => '2019-12-04 14:04:11',
                'updated_at' => '2019-12-04 14:04:11',
            ),
            9 => 
            array (
                'id' => 47,
                'center_id' => 1,
                'level_id' => 3,
                'season_id' => 29,
                'student_id' => 48,
                'year' => '2019/2020',
                'created_at' => '2019-12-04 13:59:18',
                'updated_at' => '2019-12-04 13:59:18',
            ),
            10 => 
            array (
                'id' => 46,
                'center_id' => 2,
                'level_id' => 2,
                'season_id' => 29,
                'student_id' => 47,
                'year' => '2035/2036',
                'created_at' => '2019-12-04 13:17:50',
                'updated_at' => '2019-12-04 13:17:50',
            ),
            11 => 
            array (
                'id' => 45,
                'center_id' => 2,
                'level_id' => 2,
                'season_id' => 1,
                'student_id' => 1,
                'year' => '2019/2020',
                'created_at' => '2019-12-03 15:46:39',
                'updated_at' => '2019-12-03 15:46:39',
            ),
        ));
        
        
    }
}