<?php

use Illuminate\Database\Seeder;

class StudentGradesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_grades')->delete();
        
        \DB::table('student_grades')->insert(array (
            0 => 
            array (
                'id' => 13,
                'student_id' => 1,
                'material_id' => 1,
                'center_id' => 1,
                'date' => '2019-12-03',
                'save' => '20',
                'tajweed' => '12',
                'performance' => '43',
                'total' => 100,
                'percent' => '80%',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:11:18',
            ),
        ));
        
        
    }
}