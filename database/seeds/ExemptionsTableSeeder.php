<?php

use Illuminate\Database\Seeder;

class ExemptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('exemptions')->delete();
        
        \DB::table('exemptions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ratio' => '20%',
                'reason' => 'ظروف خاصه ',
            ),
            1 => 
            array (
                'id' => 2,
                'ratio' => '20%',
                'reason' => 'ظروف خاصه ',
            ),
        ));
        
        
    }
}