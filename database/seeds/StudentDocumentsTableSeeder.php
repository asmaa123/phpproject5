<?php

use Illuminate\Database\Seeder;

class StudentDocumentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_documents')->delete();
        
        \DB::table('student_documents')->insert(array (
            0 => 
            array (
                'id' => 15,
                'student_id' => 47,
            'file' => 'img289 (1) (1) (2).jpg',
                'created_at' => '2019-12-05 10:53:20',
                'updated_at' => '2019-12-05 10:53:20',
            ),
        ));
        
        
    }
}