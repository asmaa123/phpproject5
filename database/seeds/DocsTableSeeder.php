<?php

use Illuminate\Database\Seeder;

class DocsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('docs')->delete();
        
        \DB::table('docs')->insert(array (
            0 => 
            array (
                'id' => 20,
                'file' => 'img289.jpg',
                'type_id' => 3,
                'created_at' => '2019-11-12 13:02:00',
                'updated_at' => '2019-11-12 13:02:00',
            ),
            1 => 
            array (
                'id' => 27,
                'file' => 'img289.jpg',
                'type_id' => 2,
                'created_at' => '2019-12-05 09:14:08',
                'updated_at' => '2019-12-05 09:14:08',
            ),
            2 => 
            array (
                'id' => 28,
                'file' => 'img289.jpg',
                'type_id' => 2,
                'created_at' => '2019-12-05 09:14:08',
                'updated_at' => '2019-12-05 09:14:08',
            ),
            3 => 
            array (
                'id' => 29,
                'file' => 'img289.jpg',
                'type_id' => 1,
                'created_at' => '2019-12-05 09:14:25',
                'updated_at' => '2019-12-05 09:14:25',
            ),
            4 => 
            array (
                'id' => 32,
            'file' => 'laimannung-GLrm3MJXxcI-unsplash (1).jpg',
                'type_id' => 2,
                'created_at' => '2019-12-05 09:17:08',
                'updated_at' => '2019-12-05 09:17:08',
            ),
            5 => 
            array (
                'id' => 21,
                'file' => 'asmaaabozied.pdf',
                'type_id' => 4,
                'created_at' => '2019-11-12 13:02:51',
                'updated_at' => '2019-11-12 13:02:51',
            ),
            6 => 
            array (
                'id' => 34,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'type_id' => 2,
                'created_at' => '2019-12-09 09:37:47',
                'updated_at' => '2019-12-09 09:37:47',
            ),
        ));
        
        
    }
}