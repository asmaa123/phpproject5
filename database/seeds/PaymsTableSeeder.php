<?php

use Illuminate\Database\Seeder;

class PaymsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payms')->delete();
        
        \DB::table('payms')->insert(array (
            0 => 
            array (
                'id' => 1,
                'student_id' => 1,
                'amount' => 12,
                'remain' => 10,
                'month' => 21,
                'year' => 2019,
                'payed' => 15,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:19:41',
            ),
        ));
        
        
    }
}