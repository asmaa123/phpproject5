<?php

use Illuminate\Database\Seeder;

class PayDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pay_details')->delete();
        
        \DB::table('pay_details')->insert(array (
            0 => 
            array (
                'id' => 1,
                'amount' => 12,
                'date' => NULL,
                'student_id' => 1,
                'pay_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:20:12',
            ),
        ));
        
        
    }
}