$(document).ready(function () {
    "use strict";
    $('.btn-responsive-nav').on('click', function () {
        $(this).parents('.wrapper').toggleClass('sidebar-collapse');
    });
});

/*File Upload 
 =========================*/
$(document).ready(function () {
    "use strict";
    $("#image").on("change", function () {
        document.getElementById("progressBar").style.width = 0 + '%';
        document.getElementById("progressBar").innerHTML = 0 + '%';
        $('.total-upload').html('');
        $("#status").html('');
        $(this).next("p").text($(this).val());
    });
});



/* Upload Progress Bar Script
 Script written by Adam Khoury @ DevelopPHP.com */
/* Video Tutorial: http://www.youtube.com/watch?v=EraNFJiY0Eg 
 ===============================*/
$(document).ready(function () {

    

    "use strict";
    
    var url=$('#url').val();
    var storage=$('#storage').val();
    function get_elment(el) {
        return document.getElementById(el);
    }
    function progressHandler(event) {
        var percent = (event.loaded / event.total) * 100;
        var rounPercent = get_elment("progressBar").value = Math.round(percent);
        get_elment("progressBar").style.width = rounPercent + '%';
        get_elment("progressBar").innerHTML = rounPercent + '%';
    }
    function completeHandler(event) {
        get_elment("status").innerHTML = "<div class='callout callout-info pull-right'>تم تحميل الملف بنجاح </div>";
        get_elment("progressBar").value = 0;
        $("#upload-btn").removeAttr("disabled");
//        cons
//        $("#img").val(event.success);

//        ;
//        alert(event.success);
    }
    function errorHandler(event) {
        get_elment("status").innerHTML="";
        $("#upload-btn").removeAttr("disabled");
    }
    function abortHandler(event) {
        get_elment("status").innerHTML = "Upload Aborted";
    }
    function uploadFile() {
        get_elment("status").innerHTML ="";
        var file = get_elment("image").files[0],
            formdata = new FormData(),
            ajax = new XMLHttpRequest();
        var fileName = get_elment("image").value;
        var allowed_extensions = new Array("jpg","png","gif","pdf");
        var file_extension = fileName.split('.').pop(); 
        
            if(allowed_extensions.includes(file_extension))
               {
                formdata.append("image", file);
                formdata.append("_token", $('[name="csrf_token"]').attr('content'));
                formdata.append("storage", storage);
                ajax.upload.addEventListener("progress", progressHandler, false);
                ajax.addEventListener("load", completeHandler, false);
                ajax.addEventListener("error", errorHandler, false);
                ajax.addEventListener("abort", abortHandler, false);
                ajax.open("POST", url);
                ajax.send(formdata);
                ajax.onreadystatechange = function () {
                    var response=JSON.parse(this.response);
        //            console.log(response);
                    if (this.readyState == 4 && this.status == 200) {
                        $("#img").val(response.success);
                    }
                };
            }
            else
            {
                 get_elment("status").innerHTML="<div class='callout callout-danger pull-right'>يرجى تحميل ملفات  JPG,PNG,GIF,PDF</div>";
            }
        
        
    }

    $('#upload-btn').on('click', function () {
        uploadFile();
        $(this).attr("disabled", false);
    });
});


