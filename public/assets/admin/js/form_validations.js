/* This JS is only for DEMO Purposes - Extract the code that you need
-----------------------------------------------------------------*/ 
$(document).ready(function() {				
	$(".select2").select2();
	//IconForm Validations
	var $validator = $("#IconForm").validate({
		  rules: {
		    RegionType: {required: true,},
			HospitalType: {required: true,},
			RefrralType: {required: true,},
			IdentityType: {required: true,},
			IdentityNumber: {required: true,},
			PatientName: {required: true,},
			NationalityType: {required: true,},
			GenderType: {required: true,},
			CompanyType: {required: true,},
			BadgeNumber: {required: true,},
			InsuranceCompany: {required: true,},
			CoordinatorsType: {required: true,},
			CoordinatorsType: {required: true,},
			PolicyNumber: {required: true,},
			txtdate: {required: true,},
			emailfield: {required: true, email: true, minlength: 3},
		    urlfield: {required: true, minlength: 3, url: true}
		  },

			
		  invalidHandler: function (event, validator) {
			  //display error alert on form submit    
		  },

		  errorPlacement: function (error, element) { // render error placement for each input type
			  var icon = $(element).parent('.validation').children('i');
			  var parent = $(element).parent('.validation');
			  icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
			  parent.removeClass('success-control').addClass('error-control');  
		  },

		  highlight: function (element) { // hightlight error inputs
			  var parent = $(element).parent();
			  parent.removeClass('success-control').addClass('error-control'); 
		  },

		  unhighlight: function (element) { // revert the change done by hightlight
			  
		  },

		  success: function (label, element) {
			  var icon = $(element).parent('.validation').children('i');
			  var parent = $(element).parent('.validation');
			  icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
			  parent.removeClass('error-control').addClass('success-control'); 
		  },
	});
		
	$('.select2', "#IconForm").change(function () {
		$('#IconForm').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
	});		

	$('#rootwizard').bootstrapWizard({
		'tabClass': 'form-wizard',
		'onNext': function(tab, navigation, index) {
			var $valid = $("#IconForm").valid();
			if(!$valid) {
				$validator.focusInvalid();
				return false;
			}
			else{
				$('#rootwizard').find('.form-wizard').children('li').eq(index-1).addClass('complete');
				$('#rootwizard').find('.form-wizard').children('li').eq(index-1).find('.step').html('<i class="fa fa-check"></i>');	
			}
		}
	 });	 

});	
	 