/*global $*/
(function () {
    /***************************************************************************
     * Modal View Modal
     **************************************************************************/

    $(document).on('click', '.btn-modal-view', function () {
        var $this = $(this);
        var url = $this.data('url');
        var data_lang = "lang=" + $this.data('lang');
        if ($this.find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }

        var originalHtml = $this.html();
        //$this.prop('disabled', true).html('loading...');
        request(url, data_lang, function (data) {
            $this.prop('disabled', false).html(originalHtml);
            $('#common-modal').html(data).modal('toggle');
        }, function () {
            alert('Error');
        }, 'get');
    });

    $('.approveBTN').click(function (e) {
//         alert('ckdmcksc');
        // $('.approveBTN').html(loading);
        $.ajax({
            url: $(this).data('url'),
            data: {'id': $(this).data('id'), '_token': $(this).data('token')},
            method: 'POST',
            success: function (data) {
                location.reload();
            },
            error: function (data) {

            }
        });
    });

    //////////////////////////////////
    /***************************************************************************
     * Custom logging function
     * @param mixed data
     * @returns void
     **************************************************************************/
    function _(data) {
        console.log(data);
    }
    //////////////////// for send subscribtion messege //////////////////////////////
    $(document).on("submit", ".new-form", function (e) {
        e.preventDefault();
        // var $this = $(this);
//        $('#loadmodel').show();
        var url = $(this).attr('action');
        var ajaxSubmit = $(this).find('.new-submit');
        ajaxSubmit.html(loading);
        var ajaxSubmitHtml = ajaxSubmit.html();
        var altText = loading;
        var notification = 'm';
        if (ajaxSubmit.data('loading') !== undefined) {
            altText = ajaxSubmit.data('loading');
        }
        //ajaxSubmit.prop('disabled', true).html(altText);
        var formData = new FormData(this);
        if ($(this).find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        if ($(this).data('url') !== undefined) {
            url = $(this).data('url');
        }
        if ($(this).data('notification') !== undefined) {
            notification = $(this).data('notification');
        }
        request(url, formData, function (result) {
            // altText.show();
            if (result.status) {
//                $('#loadmodel').show();
                swal({title: " Done", text: result.data, type: "success"}, function () {
                    location.reload(true);
                });
            } else {
//                $('#loadmodel').show();
                swal('Error.', result.data, 'error');
            }
        });
    });
    var AddModalBtn = $('.addButton');
    // var modelName = $('.add').attr('href');
    AddModalBtn.on('click', function () {
        var AddModalForm = $(this).closest('form');
        var formData = new FormData(AddModalForm[0]);

        if (typeof tinymce !== "undefined" && tinymce.editors.length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('desc' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        request(AddModalForm.attr('action'), formData,
                // on request success handler
                        function (result) {
                            if (result.status) {
                                swal({title: "تم", text: result.data, type: "success",confirmButtonText:"أغلق",titleClass:"swal-text",closeOnConfirm:true,allowOutsideClick:true}, function () {
                                    location.reload(true);
                                });
                            } else {
                                swal({title: "خطأ", text: result.data, type: "error",confirmButtonText:"أغلق",titleClass:"swal-text",closeOnConfirm:true,allowOutsideClick:true});
                            }
                        },
                        // on request failure handler
                                function () {
                                    alert('Internal Server Error.');
                                }, function (e) {

                            var videoProgress = $('.progress-bar');

                            var progress2 = Math.round(e.loaded / e.total * 100);
                            videoProgress.css('width', progress2 + '%');
                        });
                    });

            $('.btndelet').click(function (e) {

                var txt = $('#template-modal').html();
                var url = $(this).attr('data-url');
                txt = txt.replace(new RegExp('{url}', 'g'), url);
                $('#delete-modal .modal-dialog').html(txt);
                $('#delete-modal').modal('show');
                e.preventDefault()
            });

            /***************************************************************************
             * Search input events for filtered table
             **************************************************************************/
            var tableData = $('#ajax-table');
            $(document).on('click', '#ajax-table .pagination a', function (e) {
                var $this = $(this);
                tableData.html(loading);
                $.ajax({
                    url: $this.attr('href'),
                }).done(function (data) {
                    tableData.html(data);
                }).fail(function () {
                    alert('Internal Server Error.');
                });
                e.preventDefault();
            });
            var inputSearch = $('#input-search');
            $(document).on('click', '.btn-search', function () {
                var form = $(this).closest('form');
                var search = (inputSearch.val().length) ? "/" + inputSearch.val() : "";
                tableData.html(loading);
                request(form.attr('action') + "/search" + search, null, function (data) {
                    tableData.html(data);
                }, function () {
                    alert('Internal Server Error');
                }, 'get');
            });
            /**************************************************************************
             * Actions Of Filters Buttons
             ***************************************************************************/

            $(document).on('change', '.btn-filter', function () {
                var $this = $(this);
                var filter = $this.data('filter');
                tableData.html(loading);
                var form = $this.closest('form');
                request(form.attr('action') + "/filter/" + filter, null, function (data) {
                    tableData.html(data);
                }, function () {
                    alert('Internal Server Error.');
                }, 'get');
            });


            /***************************************************************************
             * Check ALL Button For Table Rows
             ***************************************************************************/

            $(document).on('click', '#chk-all', function () {
                $('.chk-box').prop('checked', this.checked);
            });

            ///////////////////////////////////// End Admin Panel Ajax  ////////////////////////////////////////

            //////////////////////////////////////// Site Ajax  //////////////////////////////////////////////////


            /***************************************************************************
             * Custom Ajax request function
             * @param string url
             * @param mixed|FormData data
             * @param callable(data) completeHandler
             * @param callable errorHandler
             * @param callable progressHandler
             * @returns void
             **************************************************************************/
            function _(data) {
                console.log(data);
            }

            function request(url, data, completeHandler, errorHandler, progressHandler) {
                if (typeof progressHandler === 'string' || progressHandler instanceof String) {
                    method = progressHandler;
                } else {
                    method = "POST"
                }

                $.ajax({
                    url: url, //server script to process data
                    type: method,
                    xhr: function () {  // custom xhr
                        myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload && $.isFunction(progressHandler)) { // if upload property exists
                            myXhr.upload.addEventListener('progress2', progressHandler, false); // progressbar
                        }
//                                console.log(myXhr);
                        return myXhr;
                    },
                    // Ajax events
                    success: completeHandler,
                    error: errorHandler,
                    // Form data
                    data: data,
                    // Options to tell jQuery not to process data or worry about the content-type
                    cache: false,
                    contentType: false,
                    processData: false
                }, 'json');
            }

            /***********************************************************************
             * Notify with a message in shape of fancy alert
             **********************************************************************/

            function notify(status, title, msg, type) {
                status = (status == 'error' ? 'danger' : status);
                var callable = null;
                var template = null;
                var icons = {
                    'danger': 'fa-ban',
                    'success': 'fa-check',
                    'info': 'fa-info',
                    'warning': 'fa-warning'
                };
                if ($.isFunction(type)) {
                    callable = type;
                    type = 'modal';
                }

                if (!type || type == 'm') {
                    type = 'modal';
                } else if (type == 'f') {
                    type = 'flash';
                }

                template = $("#alert-" + type).html();
                template = template.replace(new RegExp('{icon}', 'g'), icons[status]);
                template = template.replace(new RegExp('{status}', 'g'), status);
                template = template.replace(new RegExp('{title}', 'g'), title);
                template = template.replace(new RegExp('{msg}', 'g'), msg);
                switch (type) {
                    case 'modal':
                        var modal = $(template).modal('toggle');
                        if ($.isFunction(callable)) {
                            modal.on("hidden.bs.modal", callable);
                        }
                        return;
                    default:
                        $('#alert-box').html(template);
                }

            }
            /***************************************************************************
             * identify Tinymce
             **************************************************************************/
            if (typeof tinymce !== "undefined") {
                /*Text area Editors
                 =========================*/

                tinymce.init({
                    selector: '.tiny-editor',
                    height: 350,
                    theme: 'modern',
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code'
                    ],
                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    content_css: '//www.tinymce.com/css/codepen.min.css'
                });

            }
        })();