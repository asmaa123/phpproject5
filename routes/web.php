<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/test101', function(){

    $user_id = Auth::guard('admins')->user()->id;
    dd();


});*/
Route::get('/lang/{locale}', ['as' => 'site.lang', 'uses' => 'LangController@postIndex']);
Route::get('/error', ['as' => 'site.error', 'uses' => 'Controller@error']);
// Route::get('/chartdata', function(){
//     return ['value' => rand(50,100)];
// })->name('chartdata');
// Route::get('/markAsRead', function(){
//     Auth::guard('admins')->user()->unreadNotifications->markAsRead();
//     return redirect()->back();
// })->name('markAsRead');

Route::get('/attachStudentPermissions', function(){
    \App\Helpers\PermissonsHelper::attachStudentPermissions();
})->name('attachStudentPermissions');

Route::group(['namespace' => 'Site'], function () {
    Route::get('/', 'Auth\AuthController@getIndex');
    Route::get('/login', 'Auth\AuthController@getIndex');
    Route::post('/login', ['as' => 'site.login', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('/logout', ['as' => 'site.logout', 'uses' => 'Auth\AuthController@logout']);

    Route::group(['middleware' => 'auth.site'], function () {
        // Route::get('/phone', ['as' => 'member.phone', 'uses' => 'Auth\AuthController@phone']);
        // Route::Post('/verify', ['as' => 'phone.verify', 'uses' => 'Auth\AuthController@verify']);
        

        Route::get('/', ['as' => 'site.home', 'uses' => 'HomeController@getIndex']);
        Route::get('/profile', ['as' => 'site.profile', 'uses' => 'HomeController@profile']);
        Route::post('/profile', ['as' => 'site.profile.edit', 'uses' => 'HomeController@editProfile']);
        Route::post('/profileimg', ['as' => 'site.profile.image', 'uses' => 'HomeController@editProfileImage']);
        Route::get('/lock', ['as' => 'site.lock', 'uses' => 'HomeController@lock']);
        Route::post('/lock', ['as' => 'site.back', 'uses' => 'HomeController@back']);

        Route::group(['prefix' => 'students'], function () {
            Route::get('/', ['as' => 'site.students', 'uses' => 'StudentsController@getIndex']);
            Route::get('/add', ['as' => 'site.students.add', 'uses' => 'StudentsController@getAdd']);
            Route::post('/add', ['as' => 'site.students.add', 'uses' => 'StudentsController@insert']);
            Route::get('/edit/{id}', ['as' => 'site.students.edit', 'uses' => 'StudentsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'site.students.edit', 'uses' => 'StudentsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'site.students.delete', 'uses' => 'StudentsController@destroy']);
            
        });

        Route::get('/ajax-course', ['uses' => 'HomeController@getCourse']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStudent']);
        Route::get('/ajax-material', ['uses' => 'HomeController@getMaterial']);
        Route::get('/ajax-type', ['uses' => 'HomeController@getType']);
        Route::get('/ajax-percent', ['uses' => 'HomeController@getPercent']);
        Route::post('/addGrade', ['as' => 'site.grades.add', 'uses' => 'HomeController@grades']);
        Route::get('/ajax-courses', ['uses' => 'HomeController@getCourses']);
        Route::get('/ajax-students', ['uses' => 'HomeController@getStudents']);
        Route::get('/ajax-materials', ['uses' => 'HomeController@getMaterials']);
        Route::get('/ajax-percents', ['uses' => 'HomeController@getPercents']);
        Route::get('/ajax-smaterial', ['uses' => 'HomeController@getSMaterial']);
        Route::get('/ajax-spercent', ['uses' => 'HomeController@getSPercent']);
        Route::get('/ajax-from', ['uses' => 'HomeController@getFrom']);
        Route::get('/ajax-to', ['uses' => 'HomeController@getTo']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStud']);
        Route::get('/ajax-group', ['uses' => 'HomeController@getGroups']);
        Route::post('/addAbsent', ['as' => 'site.absent.add', 'uses' => 'HomeController@absent']);
        Route::get('/ajax-dto', ['uses' => 'HomeController@dto']);
        Route::get('/ajax-dfrom', ['uses' => 'HomeController@dfrom']);
        Route::get('/ajax-dates', ['uses' => 'HomeController@dates']);
        Route::get('/ajax-studs', ['uses' => 'HomeController@studs']);
        Route::get('/ajax-pcourse', ['uses' => 'HomeController@getPayCourse']);
        Route::get('/ajax-pstudent', ['uses' => 'HomeController@getPayStudent']);
        Route::get('/ajax-materialprice', ['uses' => 'HomeController@getMaterialPrice']);
        Route::get('/ajax-date', ['uses' => 'HomeController@date']);
        Route::get('/ajax-process', ['uses' => 'HomeController@process']);
        Route::get('/ajax-month', ['uses' => 'HomeController@month']);
        Route::post('/payment', ['as' => 'site.payms.add', 'uses' => 'HomeController@payms']);
        Route::get('/payment', ['as' => 'site.payms', 'uses' => 'HomeController@getIndex']);
        Route::get('/pay/{id}', ['as' => 'site.student.pay', 'uses' => 'HomeController@getPay']);
        Route::post('/pay/{id}', ['as' => 'site.student.pay', 'uses' => 'HomeController@postPay']);
        Route::get('/confirm/{id}/{code}/{amount}', ['as' => 'site.student.confirm', 'uses' => 'HomeController@confirm']);
    });
    
});
 Route::get('/search', 'ReportsController@search');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::get('/', 'AuthController@getIndex');
        Route::get('/login', 'AuthController@getIndex');
        Route::post('/login', 'AuthController@postLogin')->name('admin.login');
        Route::get('/logout', 'AuthController@getLogout')->name('admin.logout');
    });


    Route::group(['middleware' => 'auth.admin'], function () {

        Route::get('/', ['as' => 'admin.home', 'uses' => 'HomeController@getIndex']);

        Route::get('/apartments', ['as' => 'admin.apartment', 'uses' => 'ApartmentController@getIndex']);

        Route::get('/addapartments', ['as' => 'admin.apartments.add', 'uses' => 'ApartmentController@create']);

        Route::post('/addapartments', ['as' => 'admin.apartments.add', 'uses' => 'ApartmentController@store']);

        Route::get('/editapartments/{id}', ['as' => 'admin.apartments.edit', 'uses' => 'ApartmentController@getEdit']);

        Route::put('/editapartments/{id}', ['as' => 'admin.apartments.edit', 'uses' => 'ApartmentController@postEdit']);

        Route::get('/deletes/{id}', ['as' => 'admin.apartments.deletes', 'uses' => 'ApartmentController@destroy']);


        Route::get('/payments', ['as' => 'admin.payments', 'uses' => 'PaymentController@getIndex']);

        Route::get('/editpayments/{id}', ['as' => 'admin.payments.edit', 'uses' => 'PaymentController@getEdit']);

        Route::put('/editpayments/{id}', ['as' => 'admin.payments.edit', 'uses' => 'PaymentController@postEdit']);

        Route::get('/delete1/{id}', ['as' => 'admin.payments.delete', 'uses' => 'PaymentController@destroy']);

        Route::get('/addpayments', ['as' => 'admin.payments.add', 'uses' => 'PaymentController@create']);

        Route::post('/addpayments', ['as' => 'admin.payments.add', 'uses' => 'PaymentController@store']);

        Route::get('/showpayments/{id}', ['as' => 'admin.payments.show', 'uses' => 'PaymentController@show']);

        Route::get('/expensess', ['as' => 'admin.expensess', 'uses' => 'ExpenceController@getIndex']);

        Route::get('/editexpenses/{id}', ['as' => 'admin.expenses.edit', 'uses' => 'ExpenceController@getedit']);

        Route::put('/editexpenses/{id}', ['as' => 'admin.expenses.edit', 'uses' => 'ExpenceController@postEdit']);

        Route::get('/delete/{id}', ['as' => 'admin.expenses.delete', 'uses' => 'ExpenceController@destroy']);

        Route::get('/addexpense', ['as' => 'admin.expenses.add', 'uses' => 'ExpenceController@create']);

        Route::post('/addexpense', ['as' => 'admin.expenses.add', 'uses' => 'ExpenceController@store']);


        Route::get('/reportexpense', ['as' => 'admin.reportexpense', 'uses' => 'ReportsController@expenses']);

        Route::get('/reportpayment', ['as' => 'admin.reports.payments', 'uses' => 'ReportsController@getIndex']);

        Route::get('/currentbalance', ['as' => 'admin.currentbalance', 'uses' => 'ReportsController@currentbalance']);






        
        Route::get('/lock', ['as' => 'admin.lock', 'uses' => 'HomeController@lock']);
        Route::post('/lock', ['as' => 'admin.back', 'uses' => 'HomeController@back']);
        Route::post('/edit', ['as' => 'admin.org.edit', 'uses' => 'HomeController@postEdit']);
        Route::get('/data', ['as' => 'admin.data', 'uses' => 'DataController@getData']);

        Route::get('/ajax-dates', ['uses' => 'HomeController@dates']);
        Route::get('/ajax-studs', ['uses' => 'HomeController@studs']);
        Route::get('/ajax-pcourse', ['uses' => 'HomeController@getPayCourse']);
        Route::get('/ajax-pstudent', ['uses' => 'HomeController@getPayStudent']);
        Route::get('/ajax-materialprice', ['uses' => 'HomeController@getMaterialPrice']);
        Route::get('/ajax-date', ['uses' => 'HomeController@date']);








        Route::group(['prefix' => 'users'], function () {
            Route::get('/', ['as' => 'admin.users', 'uses' => 'UsersController@getIndex']);
            Route::get('/add', ['as' => 'admin.user.add', 'uses' => 'UsersController@getAdd']);
            Route::post('/add', ['as' => 'admin.user.add', 'uses' => 'UsersController@insertUser']);
            Route::get('/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'UsersController@getUser']);
            Route::post('/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'UsersController@updateUser']);
            Route::get('/delete/{id}', ['as' => 'admin.user.delete', 'uses' => 'UsersController@deleteU']);
            Route::post('/active', ['as' => 'admin.user.active', 'uses' => 'UsersController@postActive']);
            Route::post('/disActive', ['as' => 'admin.user.disActive', 'uses' => 'UsersController@postDisActive']);
            Route::post('/block', ['as' => 'admin.user.block', 'uses' => 'UsersController@postBlock']);



        });
        Route::get('/message', ['as' => 'admin.message', 'uses' => 'MessageController@getIndex']);
        Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'UsersController@profile']);
        Route::get('/expenses', ['as' => 'admin.expenses', 'uses' => 'PermissionController@get']);
        Route::post('/expenses/create', ['as' => 'admin.expenses.create', 'uses' => 'PermissionController@insert']);
        Route::post('/profile', ['as' => 'admin.profile.edit', 'uses' => 'UsersController@editProfile']);
        Route::post('/profileimg', ['as' => 'admin.profile.image', 'uses' => 'UsersController@editProfileImage']);
        Route::post('/profilepass', ['as' => 'admin.profile.pass', 'uses' => 'UsersController@editProfilePass']);
        Route::get('/order', ['as' => 'admin.order', 'uses' => 'MessageController@order']);
        Route::post('/upload', ['as' => 'admin.upload.post', 'uses' => 'UploadController@getPost']);
        Route::post('/uploadIcon', ['as' => 'admin.upload.icon', 'uses' => 'UploadController@getPost2']);
        Route::post('/uploadImage', ['as' => 'admin.upload.image', 'uses' => 'UploadController@getPost3']);
        Route::post('/uploads', 'DataController@dropzoneStore')->name('admin.dropzoneStore');
        Route::post('/upload/images', ['as' => 'admin.upload.images', 'uses' => 'CatsController@getPostImages']);




    });
});