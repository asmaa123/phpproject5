<!DOCTYPE>
<html>
<head>
  <title>نظام حفاظ</title>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Favicon -->   
  <link href="img/favicon.ico" rel="shortcut icon"/>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
  <link rel="stylesheet" href="css/style.css"/>
    

</head>
<body>
<?php
  $result = isset($_GET['result']) ? $_GET['result'] : '';
  $trackid = isset($_GET['trackid']) ? $_GET['trackid'] : '';
  $PaymentID = isset($_GET['paymentid']) ? $_GET['paymentid'] : '';
  $ref = isset($_GET['ref']) ? $_GET['ref'] : '';
  $tranid = isset($_GET['tranid']) ? $_GET['tranid'] : '';
  $amount = isset($_GET['amt']) ? $_GET['amt'] : '';
  $trx_error = isset($_GET['Error']) ? $_GET['Error'] : '';
  $trx_errortext = isset($_GET['ErrorText']) ? $_GET['ErrorText'] : '';
  $postdate = isset($_GET['postdate']) ? $_GET['postdate'] : '';
  $auth = isset($_GET['auth']) ? $_GET['auth'] : '';
  $udf1 = isset($_GET['udf1']) ? $_GET['udf1'] : '';
  $udf2 = isset($_GET['udf2']) ? $_GET['udf2'] : '';
  $udf3 = isset($_GET['udf3']) ? $_GET['udf3'] : '';
  $udf4 = isset($_GET['udf4']) ? $_GET['udf4'] : '';
  $udf5 = isset($_GET['udf5']) ? $_GET['udf5'] : '';
?>
  <section class="payment-section">
    <div class="payment-bg"></div>
        <div class="box">
            <div class="box-body">
                <div class="profile-img">
                    <img src="img/logo2.png" alt="">
                </div>
                <h3>
                  <?php if($result == "CAPTURED"){?>
                    تمت عملية الدفع بنجاح
                  <?php }elseif($result == "CANCELED"){?>
                    تم الغاء عملية الدفع
                  <?php }?>
                </h3>
                <?php if($trx_error != null && $trx_errortext != null) { ?>
                  <h3>خطأ :</h3>
                  <h3><?php echo $trx_error; - $trx_errortext ?></h3>
                <?php } ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center">
                        <tbody>
                            <tr>
                                <td>رقم الدفع</td>
                                <td><?php echo $PaymentID; ?></td>
                            </tr>
                            <tr>
                                <td>تاريخ الدفع</td>
                                <td><?php echo $postdate;?></td>
                            </tr>
                            <tr>
                                <td>حالة الدفع</td>
                                <td><?php echo $result;?></td>
                            </tr>
                            <tr>
                                <td>رقم المعاملة</td>
                                <td><?php echo $tranid;?></td>
                            </tr>
                            <tr>
                                <td>المصادقة</td>
                                <td><?php echo $auth;?></td>
                            </tr>
                            <tr>
                                <td>رقم المسار</td>
                                <td><?php echo $trackid;?></td>
                            </tr>
                            <tr>
                                <td>الرقم لمرجعي</td>
                                <td><?php echo $ref;?></td>
                            </tr>
                            <tr>
                                <td>المبلغ</td>
                                <td><?php echo $amount;?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php if($result == "CAPTURED"){?>
                  <?php $url= "http://trial-hofaz.lubic-kw.com/confirm/".$udf1."/".$PaymentID."/".$amount;?>
                  <a href="<?php echo $url;?>" class="btn btn-blue">تأكيد</a>
                <?php }elseif($result == "CANCELED"){?>
                    <a class="btn btn-blue" href="http://trial-hofaz.lubic-kw.com/">عودة الى الرئيسية</a>
                <?php }?>
            </div>
        </div>
        <div class="logo">
            <img src="img/logo-black.png" alt="">
        </div>

  </section>
  



  <!--====== Javascripts & Jquery ======--> 
  <script src="js/jquery-2.1.4.min.js"></script>

</body>
</html>
